const {ccclass, property} = cc._decorator;

@ccclass
export default class ground extends cc.Component {

    @property(cc.Node)
    player: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.getComponent(cc.PhysicsBoxCollider).enabled = false;
    }

    start () {

    }

    update (dt) {
        if(this.player.position.y - 39 >= this.node.position.y) this.getComponent(cc.PhysicsBoxCollider).enabled = true;
        else if(this.player.position.y-40 < this.node.position.y-10) this.getComponent(cc.PhysicsBoxCollider).enabled = false;
    }
}
