import userinfor = require("./user");
const { ccclass, property } = cc._decorator;
declare const firebase: any;

@ccclass
export default class signup extends cc.Component {

    @property(cc.Node)
    Email: cc.Node = null

    @property(cc.Node)
    Username: cc.Node = null

    @property(cc.Node)
    Password: cc.Node = null

    @property(cc.Node)
    Alert: cc.Node = null

    /*     @property({type:cc.AudioClip})
        clickSound: cc.AudioClip = null; */

    public txtEmail: string = "";
    public txtUsername: string = "";
    public txtPassword: string = "";

    start() {
        let backBtn = new cc.Component.EventHandler();
        backBtn.target = this.node;
        backBtn.component = "signup";
        backBtn.handler = "loadMenuScene";
        cc.find("Canvas/back").getComponent(cc.Button).clickEvents.push(backBtn);

        //Game Condition
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.update({
            GameScene: "signup/login",
            EnterGame: false,
            Task_completed: 0
        })
    }

    loadMenuScene() {
        //cc.audioEngine.playEffect(this.clickSound, false);
        cc.director.loadScene("Menu");
    }

    createAlert(type, message) {
        if (type == "error") {
            /* let alertContent = this.Alert.getChildByName("alert_content");
            alertContent.getComponent(cc.Label).string = message;
            cc.log(message);
            this.Alert.opacity = 255;
            let showAction = cc.show();
            let delayAction = cc.delayTime(0.7); // Add a 1-second delay
            let fadeOutAction = cc.fadeOut(1.3);
            let hideAction = cc.hide();
            let completeAction = cc.callFunc(() => {
                this.Alert.runAction(showAction);
            });
            let sequence = cc.sequence(showAction, delayAction, fadeOutAction, completeAction, hideAction);
            this.Alert.runAction(sequence); */
            cc.log(message);
        }
    }

    signup(event, customEventData) {
        this.txtEmail = this.Email.getComponent(cc.EditBox).string;
        this.txtPassword = this.Password.getComponent(cc.EditBox).string;
        this.txtUsername = this.Username.getComponent(cc.EditBox).string;
        // You have to assign your email and password value to another variable
        var _email = this.txtEmail;
        var _password = this.txtPassword;
        var _username = this.txtUsername;
        console.log(_username);
        if (_username == '') {
            this.createAlert("error", "User name should not be blank");
        }
        firebase.auth().createUserWithEmailAndPassword(_email, _password)
            .then(function (result) {
                let username = _username.toUpperCase();
                var data = {
                    name: username,
                    email: _email,
                    password: _password,
                    life: 3,
                    coin: 0,
                    mario_id: "default",
                    pos_x: 0,
                    pos_y: 0,
                    last_x: 0,
                    last_y: 0,
                    isDead: 0
                }
                userinfor.username = username;
                userinfor.score = '0';
                var ref = firebase.database().ref('users/' + _email.split(".").join("_").replace(/@/g, "_"));
                ref.set(data);
                //cc.audioEngine.playEffect(this.clickSound, false);
                cc.director.loadScene('Room');
                cc.log("Go to select map!")
            }).catch((error) => {
                //var errorCode = error.code;
                var errorMessage = error.message;
                this.createAlert("error", errorMessage);
            });
    }

    login(event, customEventData) {
        this.txtEmail = this.Email.getComponent(cc.EditBox).string;
        this.txtPassword = this.Password.getComponent(cc.EditBox).string;
        //firebase.auth().signInWithEmailAndPassword(this.txtEmail , this.txtPassword).then(function(user){
        firebase.auth().signInWithEmailAndPassword(this.txtEmail, this.txtPassword).then((user) => {
            if (user) {
                let _user = firebase.auth().currentUser;
                cc.log("Login Success!")
                var user_ref = firebase.database().ref('users/' + firebase.auth().currentUser.email.split(".").join("_").replace(/@/g, "_"));
                user_ref.once('value').then(function (snapshot) {
                    var childData = snapshot.val();
                    userinfor.username = childData.username;
                }).then(function () {
                    //cc.audioEngine.playEffect(this.clickSound, false);
                    cc.director.loadScene('Room');
                }).catch((error) => {
                    //var errorCode = error.code;
                    var errorMessage = error.message;
                    this.createAlert("error", errorMessage);
                });
            }
        }).catch((error) => {
            //var errorCode = error.code;
            var errorMessage = error.message;
            this.createAlert("error", errorMessage);
        });
    }


    // update (dt) {}
}
