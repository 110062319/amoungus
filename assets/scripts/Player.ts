import GameMgr from "./GameMgr";
const { ccclass, property } = cc._decorator;
declare const firebase: any;
@ccclass
export default class Player extends cc.Component {

    @property()
    playerSpeed: number = 300;

    @property(cc.SpriteFrame)
    ghostFrame: cc.SpriteFrame = null;

    @property(cc.Prefab)
    bodyPrefab: cc.Prefab = null;

    @property({ type: cc.AudioClip })
    killSound: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    dieSound: cc.AudioClip = null;

    @property(cc.Node)
    mainCamera: cc.Node = null;

    @property(cc.Button)
    reportButton: cc.Button = null;

    @property(cc.Prefab)
    chatBoardPrefab: cc.Prefab = null;

    @property(cc.Node)
    VotingScene: cc.Node = null;

    @property(cc.Label)
    voteTime1: cc.Label = null;

    @property(cc.Label)
    voteTime2: cc.Label = null;

    @property(cc.Label)
    voteBlue: cc.Label = null;
    @property(cc.Label)
    voteRed: cc.Label = null;
    @property(cc.Label)
    voteGreen: cc.Label = null;
    @property(cc.Label)
    voteBrown: cc.Label = null;
    @property(cc.Label)
    votePink: cc.Label = null;
    @property(cc.Label)
    votePurple: cc.Label = null;
    @property(cc.Label)
    voteYellow: cc.Label = null;
    @property(cc.Label)
    voteWhite: cc.Label = null;
    @property(cc.Label)
    voteSkip: cc.Label = null;

    @property(Player)
    RedPlayer: Player = null;

    @property(Player)
    BluePlayer: Player = null;

    @property(Player)
    GreenPlayer: Player = null;

    @property(Player)
    YellowPlayer: Player = null;

    @property(Player)
    BrownPlayer: Player = null;

    @property(Player)
    WhitePlayer: Player = null;

    @property(Player)
    PinkPlayer: Player = null;

    @property(Player)
    PurplePlayer: Player = null;

    @property(cc.Node)
    gameMgr: cc.Node = null;

    private physicManager: cc.PhysicsManager = null;

    private idleFrame: cc.SpriteFrame = null;
    private anim: cc.Animation = null;

    private email: string;
    private username: string;
    public current_player: string;

    private leftDown: boolean = false;
    private rightDown: boolean = false;
    private upDown: boolean = false;
    private downDown: boolean = false;
    private kDown: boolean = false;
    private rDown: boolean = false;
    private cDown: boolean = false;

    private moveDirLR = 0;
    private moveDirUD = 0;

    private closestPlayer: string = "";

    private isImpostor: boolean = false;
    private isVoting: boolean = false;
    private isDead: boolean = false;
    private isBodyGenerated: boolean = false;
    private noBody: boolean = true;

    onLoad() {
        this.physicManager = cc.director.getPhysicsManager();
        this.physicManager.enabled = true;
        this.physicManager.gravity = cc.v2(0, 0);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        this.VotingScene.active = false;
    }

    start() {
        this.idleFrame = this.getComponent(cc.Sprite).spriteFrame;
        this.anim = this.getComponent(cc.Animation);

        if (this.node.name == "PurplePlayer" || this.node.name == "RedPlayer") {
            this.isImpostor = true;
        }
        this.reportButton.interactable = false;

        //SetPlayerData
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.email = user.email.split(".").join("_").replace(/@/g, "_");

                firebase.database().ref('users/' + this.email).once('value').then(snapshot => {
                    this.username = snapshot.val().name;
                });
            }
            else { // No user is signed in.
                cc.director.loadScene("Menu");
            }
        });

        //update other player's position
        var userRef = firebase.database().ref('users/' + firebase.auth().currentUser.email.split(".").join("_").replace(/@/g, "_"));
        userRef.once('value').then(snapshot => {
            this.current_player = snapshot.val().mario_id;
            var currentNode = this.node;
            if (this.node.name != this.current_player) {
                console.log("othernode: ", this.node.name)
                var otheruserRef = firebase.database().ref('users');
                otheruserRef.once('value', function (snapshot) {
                    snapshot.forEach(childSnapshot => {
                        if (childSnapshot.val().email !== firebase.auth().currentUser.email) {
                            var anotheruserRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                            anotheruserRef.on('value', function (snapshot) {
                                if (snapshot.val().mario_id == currentNode.name) {
                                    currentNode.x = snapshot.val().pos_x;
                                    currentNode.y = snapshot.val().pos_y;
                                }
                            })
                        }
                    })
                });
            }

            // one player only vote once
            var GameCond = firebase.database().ref('GameCondition');
            GameCond.once('value', (snapshot) => {
                if (snapshot.val().GameScene == 'skeld') {
                    var VoteRef = firebase.database().ref('users');
                    VoteRef.once('value', (snapshot) => {
                        snapshot.forEach(childSnapshot => {
                            if (childSnapshot.val().mario_id == this.current_player) {
                                var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                                userRef.on('value', (snapshot) => {
                                    if (snapshot.val().has_voted == true && currentNode.name == this.current_player) {
                                        cc.log('has voted')
                                        cc.find('Canvas/Main Camera/voting_scene/red/vote_red').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/blue/vote_blue').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/green/vote_green').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/yellow/vote_yellow').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/brown/vote_brown').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/white/vote_white').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/pink/vote_pink').active = false;
                                        cc.find('Canvas/Main Camera/voting_scene/purple/vote_purple').active = false;
                                    } else if (snapshot.val().has_voted == false) {
                                        cc.find('Canvas/Main Camera/voting_scene/skip').active = true;
                                        if (!this.BluePlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/red/vote_red').active = true; }
                                        if (!this.RedPlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/blue/vote_blue').active = true; }
                                        if (!this.BrownPlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/brown/vote_brown').active = true; }
                                        if (!this.GreenPlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/green/vote_green').active = true; }
                                        if (!this.YellowPlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/yellow/vote_yellow').active = true; }
                                        if (!this.WhitePlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/white/vote_white').active = true; }
                                        if (!this.PinkPlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/pink/vote_pink').active = true; }
                                        if (!this.PurplePlayer.isDead) { cc.find('Canvas/Main Camera/voting_scene/purple/vote_purple').active = true; }
                                    }
                                })
                            }
                        })
                    });
                }
            })
        });

        //Kill Anitmation on firebase
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.once('value', (snapshot) => {
            if (snapshot.val().GameScene == 'skeld') {
                var allRef = firebase.database().ref('users');
                var currentNode = this.node;
                allRef.on('value', (snapshot) => {
                    snapshot.forEach(childSnapshot => {
                        if (childSnapshot.val().isDead == 1 && childSnapshot.val().mario_id === currentNode.name) {
                            this.playerKilled();
                            var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                            userRef.update({
                                isDead: 2       //isDead == 0: alive;  isDead == 1: playDieing; isDead == 2: already Died 
                            })
                        }
                    });
                });

                //vote condition
                var GameCond = firebase.database().ref('GameCondition');
                GameCond.on('value', (snapshot) => {
                    if (snapshot.val().Voting == true && currentNode.name == this.current_player) {
                        this.cleanBody();
                        this.Voting();
                    }
                });

                //Win/Lose judgement
                var GameCond = firebase.database().ref('GameCondition');
                GameCond.on('value', (snapshot) => {
                    if (snapshot.val().Crew_num <= snapshot.val().Impos_num && this.current_player == currentNode.name) {
                        cc.director.loadScene("impostor_win");
                    } else if (snapshot.val().Impos_num == 0 && this.current_player == currentNode.name) {
                        cc.director.loadScene("crewmate_win");
                    } else if (snapshot.val().Task_completed >= 5 && this.current_player == currentNode.name) {
                        cc.director.loadScene("crewmate_win");
                    }
                })
            }
        })

    }

    update(dt) {
        if (this.node.name == this.current_player) {
            this.node.x += this.playerSpeed * this.moveDirLR * dt;
            this.node.y += this.playerSpeed * this.moveDirUD * dt;
            this.node.scaleX = (this.moveDirLR >= 0) ? 1 : -1;
            this.node.getChildByName("PlayerName").scaleX = (this.moveDirLR >= 0) ? 1 : -1;
            if ((this.node.x != this.mainCamera.x || this.node.y != this.mainCamera.y) && !this.isVoting) {
                this.mainCamera.x = this.node.x;
                this.mainCamera.y = this.node.y;
            }
            else if (this.isVoting) {
                this.mainCamera.setPosition(0, 0);
            }
            var userRef = firebase.database().ref('users/' + firebase.auth().currentUser.email.split(".").join("_").replace(/@/g, "_"));
            userRef.update({
                pos_x: this.node.x,
                pos_y: this.node.y
            })
            this.updatePlayerDistance();
            this.updateIsBodyFound();
        }

        this.playerAnimation();
    }

    onKeyDown(event) {
        if (!this.isVoting) {
            //if(!this.isVoting){
            switch (event.keyCode) {
                case cc.macro.KEY.left:
                    this.leftDown = true;
                    this.playerMove(-1, this.moveDirUD);
                    break;
                case cc.macro.KEY.right:
                    this.rightDown = true;
                    this.playerMove(1, this.moveDirUD);
                    break;
                case cc.macro.KEY.up:
                    this.upDown = true;
                    this.playerMove(this.moveDirLR, 1);
                    break;
                case cc.macro.KEY.down:
                    this.downDown = true;
                    this.playerMove(this.moveDirLR, -1);
                    break;
                case cc.macro.KEY.k:
                    this.kDown = true;
                    break;
                case cc.macro.KEY.c:
                    this.cDown = true;
                    //this.cleanBody();
                    //cc.log(this.isDead)
                    break;
                case cc.macro.KEY.r:
                    this.rDown = true;
                    if (this.reportButton.interactable) {
                        if (this.node.name == this.current_player && !this.isDead) {
                            this.isVoting = true;
                            //Update voting condition in firebase
                            var GameCond = firebase.database().ref('GameCondition');
                            GameCond.update({
                                Voting: true
                            })
                        }
                    }
                    break;
            }
        }
    }

    onKeyUp(event) {
        if (!this.isVoting) {
            //if(!this.isVoting){
            switch (event.keyCode) {
                case cc.macro.KEY.left:
                    this.leftDown = false;
                    if (this.rightDown)
                        this.playerMove(1, this.moveDirUD);
                    else
                        this.playerMove(0, this.moveDirUD);
                    break;
                case cc.macro.KEY.right:
                    this.rightDown = false;
                    if (this.leftDown)
                        this.playerMove(-1, this.moveDirUD);
                    else
                        this.playerMove(0, this.moveDirUD);
                    break;
                case cc.macro.KEY.up:
                    this.upDown = false;
                    if (this.downDown)
                        this.playerMove(this.moveDirLR, -1);
                    else
                        this.playerMove(this.moveDirLR, 0);
                    break;
                case cc.macro.KEY.down:
                    this.downDown = false;
                    if (this.upDown)
                        this.playerMove(this.moveDirLR, 1);
                    else
                        this.playerMove(this.moveDirLR, 0);
                    break;
                case cc.macro.KEY.k:
                    this.kDown = false;
                    break;
                case cc.macro.KEY.r:
                    this.rDown = false;
                    break;
                case cc.macro.KEY.c:
                    this.cDown = false;
                    break;
            }
        }
    }

    countPlayerNum() {
        //counts player num
        var allRef = firebase.database().ref('users');
        allRef.on('value', (snapshot) => {
            var crew_num = 6;
            var impos_num = 2;
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == 'RedPlayer' || childSnapshot.val().mario_id == 'PurplePlayer') {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.on('value', (snpashot) => {
                        if (snpashot.val().isDead !== 0) {
                            impos_num -= 1;
                        }
                    })
                } else {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.on('value', (snpashot) => {
                        if (snpashot.val().isDead !== 0) {
                            crew_num -= 1;
                        }
                    })
                }
            });
            var GameCond = firebase.database().ref("GameCondition");
            GameCond.update({
                Crew_num: crew_num,
                Impos_num: impos_num
            })
        })
    }

    updatePlayerDistance() {
        var minDistance = Infinity;
        if (this.node.name != "RedPlayer" && !this.RedPlayer.isImpostor && !this.RedPlayer.isDead) {
            var RedPlayerX = (this.RedPlayer.node.x >= this.node.x) ? this.RedPlayer.node.x - this.node.x : this.node.x - this.RedPlayer.node.x;
            var RedPlayerY = (this.RedPlayer.node.y >= this.node.y) ? this.RedPlayer.node.y - this.node.y : this.node.y - this.RedPlayer.node.y;
            var RedPlayerDistance = cc.v2(RedPlayerX, RedPlayerY).mag();
            if (minDistance >= RedPlayerDistance) {
                minDistance = RedPlayerDistance;
                this.closestPlayer = "RedPlayer";
            }
        }
        if (this.node.name != "BluePlayer" && !this.BluePlayer.isImpostor && !this.BluePlayer.isDead) {
            var BluePlayerX = (this.BluePlayer.node.x >= this.node.x) ? this.BluePlayer.node.x - this.node.x : this.node.x - this.BluePlayer.node.x;
            var BluePlayerY = (this.BluePlayer.node.y >= this.node.y) ? this.BluePlayer.node.y - this.node.y : this.node.y - this.BluePlayer.node.y;
            var BluePlayerDistance = cc.v2(BluePlayerX, BluePlayerY).mag();
            if (minDistance >= BluePlayerDistance) {
                minDistance = BluePlayerDistance;
                this.closestPlayer = "BluePlayer";
            }
        }
        if (this.node.name != "GreenPlayer" && !this.GreenPlayer.isImpostor && !this.GreenPlayer.isDead) {
            var GreenPlayerX = (this.GreenPlayer.node.x >= this.node.x) ? this.GreenPlayer.node.x - this.node.x : this.node.x - this.GreenPlayer.node.x;
            var GreenPlayerY = (this.GreenPlayer.node.y >= this.node.y) ? this.GreenPlayer.node.y - this.node.y : this.node.y - this.GreenPlayer.node.y;
            var GreenPlayerDistance = cc.v2(GreenPlayerX, GreenPlayerY).mag();
            if (minDistance >= GreenPlayerDistance) {
                minDistance = GreenPlayerDistance;
                this.closestPlayer = "GreenPlayer";
            }
        }
        if (this.node.name != "YellowPlayer" && !this.YellowPlayer.isImpostor && !this.YellowPlayer.isDead) {
            var YellowPlayerX = (this.YellowPlayer.node.x >= this.node.x) ? this.YellowPlayer.node.x - this.node.x : this.node.x - this.YellowPlayer.node.x;
            var YellowPlayerY = (this.YellowPlayer.node.y >= this.node.y) ? this.YellowPlayer.node.y - this.node.y : this.node.y - this.YellowPlayer.node.y;
            var YellowPlayerDistance = cc.v2(YellowPlayerX, YellowPlayerY).mag();
            if (minDistance >= YellowPlayerDistance) {
                minDistance = YellowPlayerDistance;
                this.closestPlayer = "YellowPlayer";
            }
        }
        if (this.node.name != "BrownPlayer" && !this.BrownPlayer.isImpostor && !this.BrownPlayer.isDead) {
            var BrownPlayerX = (this.BrownPlayer.node.x >= this.node.x) ? this.BrownPlayer.node.x - this.node.x : this.node.x - this.BrownPlayer.node.x;
            var BrownPlayerY = (this.BrownPlayer.node.y >= this.node.y) ? this.BrownPlayer.node.y - this.node.y : this.node.y - this.BrownPlayer.node.y;
            var BrownPlayerDistance = cc.v2(BrownPlayerX, BrownPlayerY).mag();
            if (minDistance >= BrownPlayerDistance) {
                minDistance = BrownPlayerDistance;
                this.closestPlayer = "BrownPlayer";
            }
        }
        if (this.node.name != "WhitePlayer" && !this.WhitePlayer.isImpostor && !this.WhitePlayer.isDead) {
            var WhitePlayerX = (this.WhitePlayer.node.x >= this.node.x) ? this.WhitePlayer.node.x - this.node.x : this.node.x - this.WhitePlayer.node.x;
            var WhitePlayerY = (this.WhitePlayer.node.y >= this.node.y) ? this.WhitePlayer.node.y - this.node.y : this.node.y - this.WhitePlayer.node.y;
            var WhitePlayerDistance = cc.v2(WhitePlayerX, WhitePlayerY).mag();
            if (minDistance >= WhitePlayerDistance) {
                minDistance = WhitePlayerDistance;
                this.closestPlayer = "WhitePlayer";
            }
        }
        if (this.node.name != "PinkPlayer" && !this.PinkPlayer.isImpostor && !this.PinkPlayer.isDead) {
            var PinkPlayerX = (this.PinkPlayer.node.x >= this.node.x) ? this.PinkPlayer.node.x - this.node.x : this.node.x - this.PinkPlayer.node.x;
            var PinkPlayerY = (this.PinkPlayer.node.y >= this.node.y) ? this.PinkPlayer.node.y - this.node.y : this.node.y - this.PinkPlayer.node.y;
            var PinkPlayerDistance = cc.v2(PinkPlayerX, PinkPlayerY).mag();
            if (minDistance >= PinkPlayerDistance) {
                minDistance = PinkPlayerDistance;
                this.closestPlayer = "PinkPlayer";
            }
        }
        if (this.node.name != "PurplePlayer" && !this.PurplePlayer.isImpostor && !this.PurplePlayer.isDead) {
            var PurplePlayerX = (this.PurplePlayer.node.x >= this.node.x) ? this.PurplePlayer.node.x - this.node.x : this.node.x - this.PurplePlayer.node.x;
            var PurplePlayerY = (this.PurplePlayer.node.y >= this.node.y) ? this.PurplePlayer.node.y - this.node.y : this.node.y - this.PurplePlayer.node.y;
            var PurplePlayerDistance = cc.v2(PurplePlayerX, PurplePlayerY).mag();
            if (minDistance >= PurplePlayerDistance) {
                minDistance = PurplePlayerDistance;
                this.closestPlayer = "PurplePlayer";
            }
        }
    }

    updateIsBodyFound() {
        //if(!this.reportButton.interactable){
        // test
        /* var TestBodyPosition = cc.find("Canvas/TestBody").getPosition();
        if((TestBodyPosition.x >= this.mainCamera.x - 480 && TestBodyPosition.x <= this.mainCamera.x + 480) && (TestBodyPosition.y >= this.mainCamera.y - 270 && TestBodyPosition.y <= this.mainCamera.y + 270)){
            this.reportButton.interactable = true;
        } */
        // official gameplay
        if (this.RedPlayer.isBodyGenerated) {
            var RedBodyPosition = cc.find("Canvas/RedBody").getPosition();
            if ((RedBodyPosition.x >= this.mainCamera.x - 480 && RedBodyPosition.x <= this.mainCamera.x + 480) && (RedBodyPosition.y >= this.mainCamera.y - 270 && RedBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.BluePlayer.isBodyGenerated) {
            var BlueBodyPosition = cc.find("Canvas/BlueBody").getPosition();
            if ((BlueBodyPosition.x >= this.mainCamera.x - 480 && BlueBodyPosition.x <= this.mainCamera.x + 480) && (BlueBodyPosition.y >= this.mainCamera.y - 270 && BlueBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.GreenPlayer.isBodyGenerated) {
            var GreenBodyPosition = cc.find("Canvas/GreenBody").getPosition();
            if ((GreenBodyPosition.x >= this.mainCamera.x - 480 && GreenBodyPosition.x <= this.mainCamera.x + 480) && (GreenBodyPosition.y >= this.mainCamera.y - 270 && GreenBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.YellowPlayer.isBodyGenerated) {
            var YellowBodyPosition = cc.find("Canvas/YellowBody").getPosition();
            if ((YellowBodyPosition.x >= this.mainCamera.x - 480 && YellowBodyPosition.x <= this.mainCamera.x + 480) && (YellowBodyPosition.y >= this.mainCamera.y - 270 && YellowBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.BrownPlayer.isBodyGenerated) {
            var BrownBodyPosition = cc.find("Canvas/BrownBody").getPosition();
            if ((BrownBodyPosition.x >= this.mainCamera.x - 480 && BrownBodyPosition.x <= this.mainCamera.x + 480) && (BrownBodyPosition.y >= this.mainCamera.y - 270 && BrownBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.WhitePlayer.isBodyGenerated) {
            var WhiteBodyPosition = cc.find("Canvas/WhiteBody").getPosition();
            if ((WhiteBodyPosition.x >= this.mainCamera.x - 480 && WhiteBodyPosition.x <= this.mainCamera.x + 480) && (WhiteBodyPosition.y >= this.mainCamera.y - 270 && WhiteBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.PinkPlayer.isBodyGenerated) {
            var PinkBodyPosition = cc.find("Canvas/PinkBody").getPosition();
            if ((PinkBodyPosition.x >= this.mainCamera.x - 480 && PinkBodyPosition.x <= this.mainCamera.x + 480) && (PinkBodyPosition.y >= this.mainCamera.y - 270 && PinkBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        } else if (this.PurplePlayer.isBodyGenerated) {
            var PurpleBodyPosition = cc.find("Canvas/PurpleBody").getPosition();
            if ((PurpleBodyPosition.x >= this.mainCamera.x - 480 && PurpleBodyPosition.x <= this.mainCamera.x + 480) && (PurpleBodyPosition.y >= this.mainCamera.y - 270 && PurpleBodyPosition.y <= this.mainCamera.y + 270)) {
                this.reportButton.interactable = true;
                this.noBody = false;
            }
            else this.noBody = true;
        }
        if (this.noBody) {
            //cc.log(this.noBody);
            this.reportButton.interactable = false;
        }
        //} 
    }

    Voting() {
        //Update voting condition in firebase
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.update({
            Voting: false
        })

        //Reset Vote count in firebase
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.update({
            BluePlayer: 0,
            RedPlayer: 0,
            GreenPlayer: 0,
            YellowPlayer: 0,
            BrownPlayer: 0,
            WhitePlayer: 0,
            PinkPlayer: 0,
            PurplePlayer: 0,
            Skip: 0
        })

        //vote procedure
        this.VotingScene.active = true;
        //dead player cannot voting
        if (this.node.name == this.current_player && this.isDead) {
            cc.log('dead player')
            cc.find('Canvas/Main Camera/voting_scene/red/vote_red').active = false;
            cc.find('Canvas/Main Camera/voting_scene/blue/vote_blue').active = false;
            cc.find('Canvas/Main Camera/voting_scene/green/vote_green').active = false;
            cc.find('Canvas/Main Camera/voting_scene/yellow/vote_yellow').active = false;
            cc.find('Canvas/Main Camera/voting_scene/brown/vote_brown').active = false;
            cc.find('Canvas/Main Camera/voting_scene/white/vote_white').active = false;
            cc.find('Canvas/Main Camera/voting_scene/pink/vote_pink').active = false;
            cc.find('Canvas/Main Camera/voting_scene/purple/vote_purple').active = false;
        }
        var countdown: string = '20';
        // maintain position
        if (this.node.name == "RedPlayer") {
            this.node.setPosition(0, 95);
        }
        else if (this.node.name == "BluePlayer") {
            this.node.setPosition(100, 65);
        }
        else if (this.node.name == "GreenPlayer") {
            this.node.setPosition(145, -20);
        }
        else if (this.node.name == "YellowPlayer") {
            this.node.setPosition(110, -20);
        }
        else if (this.node.name == "BrownPlayer") {
            this.node.setPosition(-5, -170);
        }
        else if (this.node.name == "WhitePlayer") {
            this.node.setPosition(-130, -120);
        }
        else if (this.node.name == "PinkPlayer") {
            this.node.setPosition(-165, -30);
        }
        else if (this.node.name == "PurplePlayer") {
            this.node.setPosition(-120, 60);
        }
        // control candidate
        if (this.BluePlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/blue/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/blue/vote_blue").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/blue/die_blue").active = false;

        }
        if (this.RedPlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/red/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/red/vote_red").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/red/die_blue").active = false;
        }

        if (this.BrownPlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/brown/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/brown/vote_brown").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/brown/die_blue").active = false;
        }
        if (this.GreenPlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/green/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/green/vote_green").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/green/die_blue").active = false;
        }
        if (this.YellowPlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/yellow/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/yellow/vote_yellow").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/yellow/die_blue").active = false;
        }
        if (this.WhitePlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/white/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/white/vote_white").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/white/die_blue").active = false;
        }
        if (this.PinkPlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/pink/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/pink/vote_pink").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/pink/die_blue").active = false;
        }
        if (this.PurplePlayer.isDead) {
            cc.find("Canvas/Main Camera/voting_scene/purple/die_blue").active = true;
            cc.find("Canvas/Main Camera/voting_scene/purple/vote_purple").active = false;
        }
        else {
            cc.find("Canvas/Main Camera/voting_scene/purple/die_blue").active = false;
        }
        // vote
        // todo: count the vote, if 3 vote to red, this.voteRed.string = '3';
        // time countdown
        this.voteTime1.string = countdown;
        this.voteTime2.string = countdown;
        this.schedule(function () {
            this.voteTime1.string = Number(this.voteTime1.string) - 1;
            this.voteTime2.string = Number(this.voteTime2.string) - 1;
            //handle vote result display
            var VoteRef = firebase.database().ref('Vote_Count');
            VoteRef.once('value', (snapshot) => {
                this.voteRed.string = String(snapshot.val().RedPlayer)
                this.voteBlue.string = String(snapshot.val().BluePlayer)
                this.voteGreen.string = String(snapshot.val().GreenPlayer)
                this.votePink.string = String(snapshot.val().PinkPlayer)
                this.votePurple.string = String(snapshot.val().PurplePlayer)
                this.voteWhite.string = String(snapshot.val().WhitePlayer)
                this.voteBrown.string = String(snapshot.val().BrownPlayer)
                this.voteYellow.string = String(snapshot.val().YellowPlayer)
                this.voteSkip.string = String(snapshot.val().Skip)
            })
        }, 1, 15);
        this.scheduleOnce(function () {
            // todo: call vote out function playerVotedOut()
            //Reset Vote condition of Each player, one player can only vote once
            var allRef = firebase.database().ref('users');
            allRef.once('value', function (snapshot) {
                snapshot.forEach(childSnapshot => {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: false
                    })
                })
            });
            this.voteRed.string = '0';
            this.voteBlue.string = '0';
            this.voteBrown.string = '0';
            this.voteWhite.string = '0';
            this.voteYellow.string = '0';
            this.votePink.string = '0';
            this.votePurple.string = '0';
            this.voteGreen.string = '0';
            this.voteSkip.string = '0';


            this.VotingScene.active = false;
            this.cleanBody();
            this.isVoting = false;
            this.VoteResult();
        }, 20); // 30-20=10sec for show the vote results
    }

    VoteResult() {
        var VoteRef = firebase.database().ref('Vote_Count');
        var currentNode = this.node;
        VoteRef.once('value', (snapshot) => {
            var voteData = snapshot.val();

            var candidates = [
                { name: 'RedPlayer', votes: voteData.RedPlayer || 0 },
                { name: 'BluePlayer', votes: voteData.BluePlayer || 0 },
                { name: 'GreenPlayer', votes: voteData.GreenPlayer || 0 },
                { name: 'YellowPlayer', votes: voteData.YellowPlayer || 0 },
                { name: 'BrownPlayer', votes: voteData.BrownPlayer || 0 },
                { name: 'WhitePlayer', votes: voteData.WhitePlayer || 0 },
                { name: 'PinkPlayer', votes: voteData.PinkPlayer || 0 },
                { name: 'PurplePlayer', votes: voteData.PurplePlayer || 0 },
                { name: 'Skip', votes: voteData.Skip || 0 }
            ];

            var maxVote = Math.max(...candidates.map(candidate => candidate.votes));

            var winners = candidates.filter(candidate => candidate.votes === maxVote).map(candidate => candidate.name);

            if (winners.length > 1) {
                console.log('It\'s a tie between:', winners, 'with', maxVote, 'votes');
            } else {
                //cc.find('Canvas/' + winners[0]).destroy();
                var killedPlayer = "";
                if (winners[0] == 'RedPlayer') {
                    this.RedPlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "RedPlayer";
                } else if (winners[0] == 'BluePlayer') {
                    this.BluePlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "BluePlayer";
                } else if (winners[0] == 'GreenPlayer') {
                    this.GreenPlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "GreenPlayer";
                } else if (winners[0] == 'YellowPlayer') {
                    this.YellowPlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "YellowPlayer";
                } else if (winners[0] == 'BrownPlayer') {
                    this.BrownPlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "BrownPlayer";
                } else if (winners[0] == 'WhitePlayer') {
                    this.WhitePlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "WhitePlayer";
                } else if (winners[0] == 'PinkPlayer') {
                    this.PinkPlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "PinkPlayer";
                } else if (winners[0] == 'PurplePlayer') {
                    this.PurplePlayer.playerKilled();
                    this.cleanBody();
                    killedPlayer = "PurplePlayer";
                } else if (winners[0] == 'Skip') {
                    killedPlayer = "Skip";
                    alert('nobody is voted');
                }
                if (killedPlayer != 'Skip') {
                    //update firebase
                    var allRef = firebase.database().ref('users');
                    allRef.once('value', function (snapshot) {
                        snapshot.forEach(childSnapshot => {
                            if (childSnapshot.val().mario_id == killedPlayer) {
                                var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                                userRef.update({
                                    isDead: 2
                                })
                            }
                        })
                    });
                }
                console.log('Winner:', winners[0], 'with', maxVote, 'votes');
            }
        });
    }


    playerMove(moveDirLR: number, moveDirUD: number) {
        this.moveDirLR = moveDirLR;
        this.moveDirUD = moveDirUD;
    }

    playerKilled() {
        this.isDead = true;
        this.anim.stop();
        this.getComponent(cc.Sprite).spriteFrame = this.ghostFrame;
        this.generateBody();
        this.countPlayerNum();
        if (this.node.name == this.current_player) {
            this.mainCamera.getChildByName("Spotlight").active = false;
        }
    }

    playerAnimation() {
        if (this.isVoting) {
            this.anim.stop();
            this.getComponent(cc.Sprite).spriteFrame = this.idleFrame;
            //cc.log("helloooooo")
        } else if (!this.isDead) {
            //cc.log("xxxxoooooo")
            if (this.moveDirLR == 0 && this.moveDirUD == 0) {
                this.anim.stop();
                this.getComponent(cc.Sprite).spriteFrame = this.idleFrame;
                //cc.log("12341235")
            } else {
                if (this.node.name == "RedPlayer") {
                    if (!this.anim.getAnimationState("red_walk").isPlaying) {
                        this.anim.play("red_walk");
                    }
                } else if (this.node.name == "BluePlayer") {
                    if (!this.anim.getAnimationState("blue_walk").isPlaying) {
                        this.anim.play("blue_walk");
                    }
                } else if (this.node.name == "GreenPlayer") {
                    if (!this.anim.getAnimationState("green_walk").isPlaying) {
                        this.anim.play("green_walk");
                    }
                } else if (this.node.name == "YellowPlayer") {
                    if (!this.anim.getAnimationState("yellow_walk").isPlaying) {
                        this.anim.play("yellow_walk");
                    }
                } else if (this.node.name == "BrownPlayer") {
                    if (!this.anim.getAnimationState("brown_walk").isPlaying) {
                        this.anim.play("brown_walk");
                    }
                } else if (this.node.name == "WhitePlayer") {
                    if (!this.anim.getAnimationState("white_walk").isPlaying) {
                        this.anim.play("white_walk");
                    }
                } else if (this.node.name == "PinkPlayer") {
                    if (!this.anim.getAnimationState("pink_walk").isPlaying) {
                        this.anim.play("pink_walk");
                    }
                } else if (this.node.name == "PurplePlayer") {
                    //cc.log("kkkkkkkkkkkkkk")
                    if (!this.anim.getAnimationState("purple_walk").isPlaying) {
                        this.anim.play("purple_walk");
                        //cc.log("666666666666666")
                    }
                }
            }
        }
    }

    generateBody() {
        var Body = cc.instantiate(this.bodyPrefab);
        Body.setPosition(this.node.x, this.node.y);
        if (this.node.name == "RedPlayer") {
            Body.name = "RedBody";
        } else if (this.node.name == "BluePlayer") {
            Body.name = "BlueBody";
        } else if (this.node.name == "GreenPlayer") {
            Body.name = "GreenBody";
        } else if (this.node.name == "YellowPlayer") {
            Body.name = "YellowBody";
        } else if (this.node.name == "BrownPlayer") {
            Body.name = "BrownBody";
        } else if (this.node.name == "WhitePlayer") {
            Body.name = "WhiteBody";
        } else if (this.node.name == "PinkPlayer") {
            Body.name = "PinkBody";
        } else if (this.node.name == "PurplePlayer") {
            Body.name = "PurpleBody";
        }
        cc.find("Canvas").addChild(Body);
        // 将 VotingScene 的渲染顺序设置为较高的值
        var votingBoard = cc.find("Canvas/Main Camera/voting_scene");
        votingBoard.zIndex = 2;
        // 将屍體物件的渲染顺序设置为较低的值
        var bodyZIndex = 0;
        Body.zIndex = bodyZIndex;
        this.isBodyGenerated = true;
    }

    cleanBody() {
        const colors = ["Red", "Blue", "Green", "Yellow", "Brown", "White", "Pink", "Purple"];
        const canvasNode = cc.find("Canvas");

        colors.forEach(color => {
            const bodyNode = cc.find(`Canvas/${color}Body`);
            if (bodyNode) {
                bodyNode.destroy();
                this[`${color}Player`].isBodyGenerated = false;
            }
        });

        this.reportButton.interactable = false;
    }

    /*     cleanBody() {
            cc.log('cleanbody')
            // official gameplay
            if (this.RedPlayer.isBodyGenerated) {
                cc.find("Canvas/RedBody").destroy();
                this.RedPlayer.isBodyGenerated = false;
            }
            if (this.BluePlayer.isBodyGenerated) {
                cc.find("Canvas/BlueBody").destroy();
                this.BluePlayer.isBodyGenerated = false;
            }
            if (this.GreenPlayer.isBodyGenerated) {
                cc.find("Canvas/GreenBody").destroy();
                this.GreenPlayer.isBodyGenerated = false;
            }
            if (this.YellowPlayer.isBodyGenerated) {
                cc.find("Canvas/YellowBody").destroy();
                this.YellowPlayer.isBodyGenerated = false;
            }
            if (this.BrownPlayer.isBodyGenerated) {
                cc.find("Canvas/BrownBody").destroy();
                this.BrownPlayer.isBodyGenerated = false;
            }
            if (this.WhitePlayer.isBodyGenerated) {
                cc.find("Canvas/WhiteBody").destroy();
                this.WhitePlayer.isBodyGenerated = false;
            }
            if (this.PinkPlayer.isBodyGenerated) {
                cc.find("Canvas/PinkBody").destroy();
                this.PinkPlayer.isBodyGenerated = false;
            }
            if (this.PurplePlayer.isBodyGenerated) {
                cc.find("Canvas/PurpleBody").destroy();
                this.PurplePlayer.isBodyGenerated = false;
            }
    
            this.reportButton.interactable = false;
        } */

    onBeginContact(contact, selfCollider, otherCollider) {
        if (otherCollider.node.name == "Mushroom") {
            if (!this.isDead) {
                //this.playerVotedOut();
                this.playerKilled();
            }
        }
        //cc.log(otherCollider.tag);
        if ((this.isDead && otherCollider.tag == 1) || otherCollider.tag == 0) {  // 1 is the collider tag of players
            contact.disabled = true;
        }
    }

    onPreSolve(contact, selfCollider, otherCollider) {
        if (this.isImpostor && !this.isDead) {
            if (this.kDown) {
                var killedPlayer = "";
                if (otherCollider.node.name == "RedPlayer" && !this.RedPlayer.isDead && this.closestPlayer == "RedPlayer") {
                    this.RedPlayer.playerKilled();
                    killedPlayer = "RedPlayer";
                    //reduce imposter num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Impos_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Impos_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "BluePlayer" && !this.BluePlayer.isDead && this.closestPlayer == "BluePlayer") {
                    this.BluePlayer.playerKilled();
                    killedPlayer = "BluePlayer";
                    //reduce crew num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Crew_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Crew_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "GreenPlayer" && !this.GreenPlayer.isDead && this.closestPlayer == "GreenPlayer") {
                    this.GreenPlayer.playerKilled();
                    killedPlayer = "GreenPlayer";
                    //reduce crew num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Crew_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Crew_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "YellowPlayer" && !this.YellowPlayer.isDead && this.closestPlayer == "YellowPlayer") {
                    this.YellowPlayer.playerKilled();
                    killedPlayer = "YellowPlayer";
                    //reduce crew num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Crew_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Crew_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "BrownPlayer" && !this.BrownPlayer.isDead && this.closestPlayer == "BrownPlayer") {
                    this.BrownPlayer.playerKilled();
                    killedPlayer = "BrownPlayer";
                    //reduce crew num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Crew_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Crew_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "WhitePlayer" && !this.WhitePlayer.isDead && this.closestPlayer == "WhitePlayer") {
                    this.WhitePlayer.playerKilled();
                    killedPlayer = "WhitePlayer";
                    //reduce crew num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Crew_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Crew_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "PinkPlayer" && !this.PinkPlayer.isDead && this.closestPlayer == "PinkPlayer") {
                    this.PinkPlayer.playerKilled();
                    killedPlayer = "PinkPlayer";
                    //reduce crew num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Crew_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Crew_num: updatedValue,
                        });
                    });
                }
                if (otherCollider.node.name == "PurplePlayer" && !this.PurplePlayer.isDead && this.closestPlayer == "PurplePlayer") {
                    this.PurplePlayer.playerKilled();
                    killedPlayer = "PurplePlayer";
                    //reduce imposter num
                    var GameCond = firebase.database().ref('GameCondition');
                    GameCond.once('value').then(snapshot => {
                        var currentValue = snapshot.val().Impos_num;
                        var updatedValue = currentValue - 1;

                        GameCond.update({
                            Impos_num: updatedValue,
                        });
                    });
                }
                this.kDown = false;

                //update firebase
                var allRef = firebase.database().ref('users');
                allRef.once('value', function (snapshot) {
                    snapshot.forEach(childSnapshot => {
                        if (childSnapshot.val().mario_id == killedPlayer) {
                            var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                            userRef.update({
                                isDead: 1
                            })
                        }
                    })
                });
            }
        }
    }
}
