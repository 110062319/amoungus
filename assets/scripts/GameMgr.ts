const { ccclass, property } = cc._decorator;
import Player from "./Player";
declare const firebase: any;
@ccclass
export default class GameMgr extends cc.Component {
    @property(Player)
    player: Player = null;

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    onLoad() {
        //cc.log("Task9"+cc.find("Canvas/T9/Task9").active)
        cc.find("Canvas/T1/Task1").active = false;
        cc.find("Canvas/T2/Task2").active = false;
        cc.find("Canvas/T3/Task3").active = false;
        cc.find("Canvas/T4/Task4").active = false;
        cc.find("Canvas/T5/Task5").active = false;
        cc.find("Canvas/T6/Task6").active = false;
        cc.find("Canvas/T7/Task7").active = false;
        cc.find("Canvas/T8/Task8").active = false;
        cc.find("Canvas/T9/Task9").active = false;
        //cc.find("Canvas/T10/map").active = false;
    }

    update(dt) {
        if (!cc.audioEngine.isMusicPlaying()) cc.audioEngine.playMusic(this.bgm, true);
    }

    Voting() {
        /* this.voting.active = true;
        cc.log("vote"); */
    }

    Vote_Red() {
        alert('vote_red')
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('RedPlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                RedPlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
    }
    Vote_Blue() {
        alert('vote_blue')
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('BluePlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                BluePlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
    }
    Vote_Green() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('GreenPlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;


            VoteRef.update({
                GreenPlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote_green')
    }
    Vote_Yellow() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('YellowPlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                YellowPlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote_yellow')
    }
    Vote_Brown() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('BrownPlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                BrownPlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote_Brown')
    }
    Vote_White() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('WhitePlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                WhitePlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote_White')
    }
    Vote_Pink() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('PinkPlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                PinkPlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote_Pink')
    }
    Vote_Purple() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('PurplePlayer').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                PurplePlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote_Purple')
    }

    SkipVoting() {
        var VoteRef = firebase.database().ref('Vote_Count');
        VoteRef.child('Skip').once('value').then(snapshot => {
            var currentValue = snapshot.val();
            var updatedValue = currentValue + 1;

            VoteRef.update({
                PurplePlayer: updatedValue,
            });
        });
        var nowPlayer = this.player.current_player;
        var allRef = firebase.database().ref('users');
        allRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                if (childSnapshot.val().mario_id == nowPlayer) {
                    var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                    userRef.update({
                        has_voted: true
                    })
                }
            });
        });
        alert('vote Skip')
    }

    task1Scene() {
        var task1 = cc.find("Canvas/T1/Task1");
        var current1 = cc.find("Canvas/" + this.player.current_player);
        task1.x = current1.x - 221;
        task1.y = current1.y - 417;
        task1.active = true;
    }

    task2Scene() {
        var task2 = cc.find("Canvas/T2/Task2");
        var current2 = cc.find("Canvas/" + this.player.current_player);
        task2.x = current2.x - 236;
        task2.y = current2.y - 192;
        task2.active = true;
    }

    task3Scene() {
        var task3 = cc.find("Canvas/T3/Task3");
        var current3 = cc.find("Canvas/" + this.player.current_player);
        task3.x = current3.x - 164;
        task3.y = current3.y - 240;
        task3.active = true;
    }

    task4Scene() {
        var task4 = cc.find("Canvas/T4/Task4");
        var current4 = cc.find("Canvas/" + this.player.current_player);
        task4.x = current4.x - 312;
        task4.y = current4.y - 17;
        task4.active = true;
    }

    task5Scene() {
        var task5 = cc.find("Canvas/T5/Task5");
        var current5 = cc.find("Canvas/" + this.player.current_player);
        task5.x = current5.x - 285;
        task5.y = current5.y - 70;
        task5.active = true;
    }

    task6Scene() {
        var task6 = cc.find("Canvas/T6/Task6");
        var current6 = cc.find("Canvas/" + this.player.current_player);
        task6.x = current6.x - 130;
        task6.y = current6.y + 100;
        task6.active = true;
    }

    task7Scene() {
        var task7 = cc.find("Canvas/T7/Task7");
        var current7 = cc.find("Canvas/" + this.player.current_player);
        task7.x = current7.x - 807;
        task7.y = current7.y - 44;
        task7.active = true;
    }

    task8Scene() {
        var task8 = cc.find("Canvas/T8/Task8");
        var current8 = cc.find("Canvas/" + this.player.current_player);
        task8.x = current8.x - 1096;
        task8.y = current8.y + 77;
        task8.active = true;
    }

    task9Scene() {
        var task9 = cc.find("Canvas/T9/Task9");
        var current9 = cc.find("Canvas/" + this.player.current_player);
        task9.x = current9.x - 2105;
        task9.y = current9.y + 234;
        task9.active = true;
    }

    /*     task10Scene() {
            var task10 = cc.find("Canvas/T10/map");
            var current10 = cc.find("Canvas/"+this.player.current_player);
            cc.log("OKKKKKKKKKKKKKKKK")
            task10.x = current10.x + 117.857;
            task10.y = current10.y + 276.708;
            task10.active = true; 
        } */

    task11Scene() {
        var task11 = cc.find("Canvas/T11/Task11");
        var current11 = cc.find("Canvas/" + this.player.current_player);
        task11.x = current11.x - 2105;
        task11.y = current11.y + 234;
        task11.active = true;
    }
    // update (dt) {}
    start() {
        var userRef = firebase.database().ref('GameCondition');
        userRef.update({
            GameScene: 'skeld'
        })
        //cc.log(firebase.auth().currentUser.email)
    }

}