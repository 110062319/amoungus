import game from "./game";

const {ccclass, property} = cc._decorator;

@ccclass
export default class start extends cc.Component {

    @property(cc.Label)
    enter1: cc.Label = null;

    @property(cc.Label)
    enter2: cc.Label = null;

    @property(cc.Label)
    enter3: cc.Label = null;

    @property(cc.Label)
    dot1: cc.Label = null;

    @property(cc.Label)
    dot2: cc.Label = null;

    @property(cc.Label)
    beatking: cc.Label = null;

    @property(cc.Label)
    back: cc.Label = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    private timer: number = 0;
    private duration: number = 2;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.find("Canvas/T10/Task10/game").active = true;
        this.enter1.node.opacity = 0;
        this.enter2.node.opacity = 0;
        this.enter3.node.opacity = 0;
        this.dot1.node.opacity = 0;
        this.dot2.node.opacity = 0;
        this.beatking.node.opacity = 0;
        this.back.node.opacity = 0;
        this.schedule(function(){this.timer += 1}, 1, 17);
    }

    start () {
        cc.audioEngine.playMusic(this.bgm, true);
    }

    update (dt) {
        this.word();
        if(this.timer == 16) this.fadeout();

    }

    word(){
        const targetOpacity = 255;
        const fadeDuration = this.duration;
        if(this.timer >= 1 && this.timer < 3){
            this.enter1.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
        else if(this.timer >= 3 && this.timer < 5){
            this.enter2.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
        else if(this.timer >= 5 && this.timer < 7){
            this.dot1.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
        else if(this.timer >= 7 && this.timer < 9){
            this.enter3.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
        else if(this.timer >= 9 && this.timer < 11){
            this.beatking.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
        else if(this.timer >= 11 && this.timer < 13){
            this.back.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
        else if(this.timer >= 13 && this.timer < 15){
            this.dot2.node.runAction(
                cc.fadeTo(fadeDuration, targetOpacity)
            );
        }
    }

    fadeout(){
        this.enter1.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.enter2.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.enter3.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.dot1.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.dot2.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.beatking.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.back.node.runAction(
            cc.fadeTo(2, 0)
        );
        this.scheduleOnce(() => {
            /* cc.log(cc.find("Canvas/T10/Task10/start").active);
            cc.log(cc.find("Canvas/T10/Task10/game").active);
            cc.find("Canvas/T10/Task10/game").active = true;
            cc.log("OKKKKKKKKKKKKKKKK")
            cc.find("Canvas/T10/Task10/start").destroy();
            cc.log("NOOOOOOOOOOOOOOOOOOOOOOOOOOOO") */
            
            
        }, 2.5);
    }
}
