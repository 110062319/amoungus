// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
const {ccclass, property} = cc._decorator;

@ccclass
export default class boss extends cc.Component {
    
    @property(cc.Node)
    player: cc.Node = null;

    @property(cc.Label)
    hp: cc.Label = null;

    @property(cc.Prefab)
    blackhole: cc.Prefab = null;

    @property(cc.Node)
    ending: cc.Node = null;

    @property(cc.Node)
    blood1: cc.Node = null;

    @property(cc.Node)
    blood2: cc.Node = null;

    @property(cc.Node)
    blood3: cc.Node = null;

    @property(cc.Node)
    magic1: cc.Node = null;

    @property(cc.Node)
    magic2: cc.Node = null;

    @property(cc.Node)
    magic3: cc.Node = null;

    @property(cc.Node)
    magic4: cc.Node = null;

    @property(cc.Node)
    magic5: cc.Node = null;
    
    @property(cc.Node)
    wasd: cc.Node = null;
    
    @property(cc.Node)
    l: cc.Node = null;

    @property(cc.AudioClip)
    attackAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    endbgm: cc.AudioClip = null;

    @property(cc.Canvas)
    canvas: cc.Canvas = null;

    private moveSpeed: number = 50;
    lv: cc.Vec2;
    private value: number = 9;
    timer: number = 0;
    endAni: cc.Animation;

    onLoad () {
        this.resetScore();
        this.blood1.active = false;
        this.blood2.active = false;
        this.blood3.active = false;
        this.ending.active = false;

        this.magic1.active = false;
        this.magic2.active = false;
        this.magic3.active = false;
        this.magic4.active = false;
        this.magic5.active = false;
    }

    public resetScore()
    {
        this.value = 9;
        this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛 💛 💛";
    }


    start () {

    }

    update (dt) {
        const playerPos = this.player.position;
        const bossPos = this.node.position;
        let scaleX = Math.abs(this.node.scaleX);
        if(this.value > 6) this.moveSpeed = 50;
        else if(this.value < 7 && this.value > 3) this.moveSpeed = 70;
        else if(this.value < 4) this.moveSpeed = 90;

        if(this.value > 6) this.timer = 0;
        else if(this.value < 7) this.timer += dt;

        if(this.value < 4){
            this.magicset();
            if(this.timer >= 2){
                this.timer = 0;
                this.blackattack();
            }
        }
        else if(this.value < 7 && this.value > 3){
            if(this.timer >= 0 && this.timer < 1){
                this.magicA();
            }
            else if(this.timer >= 1 && this.timer < 2){
                this.magicB();
            }
            else if(this.timer >= 2 && this.timer < 3){
                this.magicC();
            }
            else if(this.timer >= 3 && this.timer < 4){
                this.magicD();
            }
            else if(this.timer >= 4 && this.timer < 5){
                this.magicE();
            }
            else if(this.timer >= 5) this.timer = 0;
        }
        
        const horizontalDirection = playerPos.x > bossPos.x ? 1 : -1;
        if(playerPos.x == bossPos.x){
            //this.node.scaleX = -scaleX;
            this.moveSpeed = 0;
        }
        else{
            if(horizontalDirection == 1){
                this.node.scaleX = -scaleX;
            }
            else if(horizontalDirection == -1){
                this.node.scaleX = scaleX;
            }
        }
        this.lv = this.node.getComponent(cc.RigidBody).linearVelocity;
        this.lv.x = horizontalDirection * this.moveSpeed;
        this.node.getComponent(cc.RigidBody).linearVelocity = this.lv;
    }

    magicA() {
        this.magic1.active = true;
        this.magic4.active = false;
    }
    magicB(){
        this.magic2.active = true;
        this.magic3.active = false;
    }
    magicC(){
        this.magic5.active = true;
        this.magic1.active = false;
    }
    magicD(){
        this.magic4.active = true;
        this.magic2.active = false;
    }
    magicE(){
        this.magic3.active = true;
        this.magic5.active = false;
    }

    magicset(){
        this.magic1.active = false;
        this.magic2.active = false;
        this.magic3.active = false;
        this.magic4.active = false;
        this.magic5.active = false;
    }

    blackattack() {
        const blackhole = cc.instantiate(this.blackhole);
        const startPos = this.node.getPosition();
        blackhole.setPosition(startPos);
        this.node.parent.addChild(blackhole);

        cc.audioEngine.playEffect(this.attackAudio, false);

        let moveDir = null;

        if(this.node.scaleX > 0)
            moveDir = cc.moveBy(2, cc.v2(-500, 0));
        else
            moveDir = cc.moveBy(2, cc.v2(500, 0));

        blackhole.runAction(
            cc.sequence(
                moveDir,
                cc.callFunc(() => {
                    blackhole.removeFromParent();
                })
            )
        );
    }

    blood() {
        this.blood1.active = true;
        this.blood2.active = true;
        this.blood3.active = true;
        this.scheduleOnce(() => {
            this.blood1.active = false;
            this.blood2.active = false;
            this.blood3.active = false;
        }, 0.2);
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group === 'bullet') {
            this.value --;
            this.addOnePoint();
            this.blood();
            if(this.value == 6){
                cc.director.getCollisionManager().enabled = false;
                console.log("stop");
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                    console.log("recover");
                    console.log(this.moveSpeed);
                }, 2);
            }
            else if(this.value == 3){
                cc.director.getCollisionManager().enabled = false;
                console.log("stop");
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                    console.log("recover");
                    console.log(this.moveSpeed);
                }, 2);
            }
        }
        else if(other.node.group === 'player'){
            if(this.value < 6) this.value ++;
            this.addOnePoint();
        }
    }

    public addOnePoint()
    {
        if(this.value == 0){
            this.hp.getComponent(cc.Label).string = "                     VICTORY!!   ";
            this.node.destroy();
            this.ending.active = true;
            this.wasd.active = false;
            this.l.active = false;
            this.canvas.node.runAction(
                cc.fadeTo(5, 100)
            );
            cc.audioEngine.stopMusic();
            cc.audioEngine.setEffectsVolume(0);
            cc.audioEngine.playMusic(this.endbgm, false);
        }
        else if(this.value == 1){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤";
        }
        else if(this.value == 2){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤";
        }
        else if(this.value == 3){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤";
        }
        else if(this.value == 4){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎";
        }
        else if(this.value == 5){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎";
        }
        else if(this.value == 6){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎";
        }
        else if(this.value == 7){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛";
        }
        else if(this.value == 8){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛 💛";
        }
        else if(this.value == 9){
            this.hp.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛 💛 💛";
        }
    }
}
