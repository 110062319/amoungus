const {ccclass, property} = cc._decorator;

@ccclass
export default class Bullet extends cc.Component 
{
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        console.log("king!");
        // 判断碰撞的对象是否为敌人
        if (other.node.group === 'king') {
            //this.node.destroy(); // 销毁子弹节点
            this.scheduleOnce(() => {
                this.node.destroy();
            }, 0.2);
        }
    }
}
