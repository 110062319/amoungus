const {ccclass, property} = cc._decorator;

@ccclass
export default class game extends cc.Component {

    @property
    gravityX: number = 0;

    @property
    gravityY: number = -1000;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property(cc.Canvas)
    canvas: cc.Canvas = null;

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
        //cc.director.getCollisionManager().enabledDebugDraw= true;
        cc.director.getPhysicsManager().gravity = cc.v2(this.gravityX, this.gravityY);
        this.canvas.node.opacity = 0;
        
    }

    start () {
        this.playBGM();
        this.canvas.node.runAction(
            cc.fadeTo(2, 255)
        );
    }

    playBGM(){
        cc.audioEngine.playMusic(this.bgm, true);
    }

    // update (dt) {}
}
