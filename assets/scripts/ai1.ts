const {ccclass, property} = cc._decorator;
@ccclass
export default class ai1 extends cc.Component {
    @property(cc.Node)
    enemy: cc.Node = null;
    @property(cc.Node)
    enemy2: cc.Node = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    winSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    loseSE: cc.AudioClip = null;
    @property(cc.Label)
    Timer1: cc.Label = null;
    
    private isMovingLeft: boolean = false;
    private isMovingRight: boolean = false;
    private isMovingUp: boolean = false;
    private isMovingDown: boolean = false;
    private isGood: boolean = false;
    randNum:number;
    pMoveSpeed: number;
    eMoveSpeed: number;
    begin:number;

    start(){
        cc.audioEngine.playMusic(this.bgm,true);
        this.pMoveSpeed=200;
        this.eMoveSpeed=100;
        this.randNum = Math.floor(Math.random() *441)+50;
        this.enemy.y=this.randNum;
        this.enemy2.y=this.randNum;
        this.begin=0;
        this.schedule(function(){this.begin++}, 1);
    }

    onLoad(){
        cc.director.getPhysicsManager().enabled=true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    update(dt){
        this.Timer1.string = this.begin.toString();
        if(this.begin==21){
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.winSE, false);
            cc.find("Canvas/T8/Task8").active = false;
        }
        
        this.playerMovement(dt);
        this.enemyMovement(dt);
    }
    playerMovement(dt){
        if (this.isMovingLeft) {
            this.node.x -= this.pMoveSpeed * dt;
        } 
        if (this.isMovingRight) {
            this.node.x += this.pMoveSpeed * dt;
        }
        if (this.isMovingUp) {
            this.node.y += this.pMoveSpeed * dt;
        } 
        if (this.isMovingDown) {
            this.node.y -= this.pMoveSpeed * dt;
        }
    }
    enemyMovement(dt) {
        if(this.begin){
            const direction = this.node.position.sub(this.enemy.position);
            const distance = direction.mag();
            const normalizedDirection = direction.normalize();
            const speed = this.eMoveSpeed * dt;
            const movement = normalizedDirection.mul(speed);
            if (distance > speed) {
            this.enemy.position = this.enemy.position.add(movement);
            } else {
            this.enemy.position = this.node.position.clone();
            }
        }
    }
    onKeyDown(e){
        if (e.keyCode === cc.macro.KEY.a) {
            this.isMovingLeft = true;
        } 
        if (e.keyCode === cc.macro.KEY.d) {
            this.isMovingRight = true;
        }
        if (e.keyCode === cc.macro.KEY.w) {
            this.isMovingUp = true;
        } 
        if (e.keyCode === cc.macro.KEY.s) {
            this.isMovingDown = true;
        }
        if (e.keyCode === cc.macro.KEY.space) {
            this.isGood = true;
        }
    }
    onKeyUp(e){
        if (e.keyCode === cc.macro.KEY.a) {
            this.isMovingLeft = false;
        } 
        if (e.keyCode === cc.macro.KEY.d) {
            this.isMovingRight = false;
        }
        if (e.keyCode === cc.macro.KEY.w) {
            this.isMovingUp = false;
        } 
        if (e.keyCode === cc.macro.KEY.s) {
            this.isMovingDown = false;
        }
    }
    onBeginContact(contact,self,other){
        if(((other.node.name=="enemy1")||(other.node.name=="enemy2"))&&!this.isGood){
            cc.audioEngine.pauseMusic()
            cc.audioEngine.playEffect(this.loseSE,false);
            cc.audioEngine.resumeMusic()
            this.begin = 0;
        } 
    }
}
