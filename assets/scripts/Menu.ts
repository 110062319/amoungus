const { ccclass, property } = cc._decorator;
declare const firebase: any;

@ccclass
export default class Menu extends cc.Component {

    @property(cc.Label)
    LeaderName: cc.Label = null;

    @property(cc.Label)
    LeaderScore: cc.Label = null;

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    @property(cc.Slider)
    Volume: cc.Slider = null;

    start() {
        cc.audioEngine.playMusic(this.bgm, true)
        let startBtn = new cc.Component.EventHandler();
        startBtn.target = this.node;
        startBtn.component = "Menu";
        startBtn.handler = "loadRoomScene";
        cc.find("Canvas/start").getComponent(cc.Button).clickEvents.push(startBtn);

        let loginBtn = new cc.Component.EventHandler();
        loginBtn.target = this.node;
        loginBtn.component = "Menu";
        loginBtn.handler = "loadLoginScene";
        cc.find("Canvas/login").getComponent(cc.Button).clickEvents.push(loginBtn);

        let signUpBtn = new cc.Component.EventHandler();
        signUpBtn.target = this.node;
        signUpBtn.component = "Menu";
        signUpBtn.handler = "loadSignUpScene";
        cc.find("Canvas/signup").getComponent(cc.Button).clickEvents.push(signUpBtn);

        cc.find("Canvas/leader_label").active = false;
        cc.find("Canvas/setting_frame").active = false;

        //Game Condition
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.update({
            Scene: "menu"
        })

        var userRef = firebase.database().ref('GameCondition');
        userRef.update({
            GameScene: 'lobby',
            Crew_num: 6,
            Impos_num: 2,
            Task_completed: 0
        })
    }

    leaderboard() {
        cc.find("Canvas/leader_label").active = !cc.find("Canvas/leader_label").active;
        cc.find("Canvas/login").active = !cc.find("Canvas/login").active;
        cc.find("Canvas/signup").active = !cc.find("Canvas/signup").active;
        cc.find("Canvas/start").active = !cc.find("Canvas/start").active;

        /*  const dbRef = firebase.database().ref();
 
         dbRef.orderByChild('score').on('value', snapshot => {
         let leaderlist = [];
         snapshot.forEach(childSnapshot => {
             const user = childSnapshot.val();
             leaderlist.push(user);
         });
         var len = leaderlist.length;
         cc.log(leaderlist);
         this.LeaderName.string = leaderlist[len-1].username + "\n" + leaderlist[len-2].username + "\n" + leaderlist[len-3].username;
         this.LeaderScore.string = leaderlist[len-1].score + "\n" + leaderlist[len-2].score + "\n" + leaderlist[len-3].score;
         });  */
    }

    setting() {
        cc.find("Canvas/setting_frame").active = !cc.find("Canvas/setting_frame").active;
        cc.find("Canvas/login").active = !cc.find("Canvas/login").active;
        cc.find("Canvas/signup").active = !cc.find("Canvas/signup").active;
        cc.find("Canvas/start").active = !cc.find("Canvas/start").active;
    }
    controlVolume() {
        cc.audioEngine.setMusicVolume(this.Volume.progress);
    }

    loadRoomScene() {
        cc.director.loadScene("Room");
    }

    loadLoginScene() {
        cc.director.loadScene("Login");
    }

    loadSignUpScene() {
        cc.director.loadScene("Signup");
    }
    // update (dt) {}
}
