const {ccclass, property} = cc._decorator;

@ccclass
export default class blacktran extends cc.Component 
{
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        console.log("disapear");

        if (other.node.group === 'player'){
            this.scheduleOnce(() => {
                this.node.destroy();
            }, 1.5);
        }
    }
}
