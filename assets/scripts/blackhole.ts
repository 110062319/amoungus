const {ccclass, property} = cc._decorator;

@ccclass
export default class blackhole extends cc.Component 
{
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group === 'player'){
            this.node.destroy();
        }
    }
}
