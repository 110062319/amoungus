const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerBody extends cc.Component {

    @property(cc.SpriteFrame) 
    bodyFrame: cc.SpriteFrame = null;

    private idleFrame: cc.SpriteFrame = null;

    private anim: cc.Animation = null;

    //onLoad(){}

    start(){
        this.idleFrame = this.getComponent(cc.Sprite).spriteFrame;
        this.anim = this.getComponent(cc.Animation);
        this.bodyAnimation();
    }

    //update(dt){}

    bodyAnimation(){
        if(this.node.name == "RedBody"){
            if(!this.anim.getAnimationState("red_die").isPlaying){
                this.anim.play("red_die");
            }
        }else if(this.node.name == "BlueBody"){
            if(!this.anim.getAnimationState("blue_die").isPlaying){
                this.anim.play("blue_die");
            }
        }else if(this.node.name == "GreenBody"){
            if(!this.anim.getAnimationState("green_die").isPlaying){
                this.anim.play("green_die");
            }
        }else if(this.node.name == "YellowBody"){
            if(!this.anim.getAnimationState("yellow_die").isPlaying){
                this.anim.play("yellow_die");
            }
        }else if(this.node.name == "BrownBody"){
            if(!this.anim.getAnimationState("brown_die").isPlaying){
                this.anim.play("brown_die");
            }
        }else if(this.node.name == "WhiteBody"){
            if(!this.anim.getAnimationState("white_die").isPlaying){
                this.anim.play("white_die");
            }
        }else if(this.node.name == "PinkBody"){
            if(!this.anim.getAnimationState("pink_die").isPlaying){
                this.anim.play("pink_die");
            }
        }else if(this.node.name == "PurpleBody"){
            if(!this.anim.getAnimationState("purple_die").isPlaying){
                this.anim.play("purple_die");
            }
        }
        this.getComponent(cc.Sprite).spriteFrame = this.bodyFrame;
    }
}
