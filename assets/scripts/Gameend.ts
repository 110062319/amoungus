const { ccclass, property } = cc._decorator;
declare const firebase: any;
@ccclass
export default class NewClass extends cc.Component {

    onLoad() {
        this.scheduleOnce(function () { cc.director.loadScene("Menu"); }, 2);
    }

    start() {
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.update({
            Impos_num: 2,
            Crew_num: 6,
            EnterGame: false
        })
    }
}
