// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class hp extends cc.Component {

    private value: number = 9;

    onLoad () 
    {
        this.resetScore();
    }

    public resetScore()
    {
        this.value = 9;

        this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛 💛 💛";
    }

    public addOnePoint()
    {
        this.value--;

        if(this.value == 0){

        }
        else if(this.value == 1){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤";
        }
        else if(this.value == 2){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤";
        }
        else if(this.value == 3){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤";
        }
        else if(this.value == 4){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎";
        }
        else if(this.value == 5){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎";
        }
        else if(this.value == 6){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎";
        }
        else if(this.value == 7){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛";
        }
        else if(this.value == 8){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛 💛";
        }
        else if(this.value == 9){
            this.node.getComponent(cc.Label).string = "BOSS HP： 🖤 🖤 🖤 🤎 🤎 🤎 💛 💛 💛";
        }
    }
}
