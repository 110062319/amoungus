const {ccclass, property} = cc._decorator;
const Input = {};
const State = {
    stand: 1,
    attack: 2,
};

@ccclass
export default class player_boss extends cc.Component {
    
    _speed: number;
    sp: cc.Vec2;
    playerState: number;
    anima: string;
    lv: cc.Vec2;
    playerAni: cc.Animation;

    @property
    jumpDuration: number = 0.5;

    @property
    maxJumpCount: number = 1;

    @property(cc.AudioClip)
    jumpAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    attackAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    transferAudio: cc.AudioClip = null;

    @property(cc.AudioClip)
    deadAudio: cc.AudioClip = null;

    @property(cc.Prefab)
    bulletPrefab: cc.Prefab = null;

    @property(cc.Node)
    blacktran: cc.Node = null;
    
    bulletSpeed: number = 500; 

    private jumpCount: number = 0;
    private canJump: boolean = true;
    private canMove: boolean = true;

    intervalTime: number = 0.5;
    timer: number = 0;

    onLoad () {
        this._speed = 500;
        this.sp = cc.v2(0, 0);

        this.playerState = State.stand;
        this.anima = 'stop';
        this.playerAni = this.node.getComponent(cc.Animation);

        this.blacktran.active = false;

        cc.systemEvent.on('keydown', this.onkeydown, this);
        cc.systemEvent.on('keyup', this.onkeyup, this);

        cc.audioEngine.setMusicVolume(1);
        cc.audioEngine.setEffectsVolume(0.5);
    }

    start () {

    }

    onDestroy () {
        cc.systemEvent.off('keydown', this.onkeydown, this);
        cc.systemEvent.off('keyup', this.onkeyup, this);
    }

    onkeydown (event: cc.Event.EventKeyboard) {
        Input[event.keyCode] = 1;
    }

    onkeyup (event: cc.Event.EventKeyboard) {
        Input[event.keyCode] = 0;
    }

    jump() {
        if (this.jumpCount < this.maxJumpCount) {
            this.jumpCount++;
            this.canJump = false;

            const jumpAction = cc.jumpBy(this.jumpDuration, cc.v2(0, 0), 250, 1);
            const finishJumpAction = cc.callFunc(() => {
                this.jumpCount--;
                if (this.jumpCount === 0) {
                    this.canJump = true;
                }
            });

            const sequence = cc.sequence(jumpAction, finishJumpAction);
            this.node.runAction(sequence);

            if (this.jumpAudio) {
                cc.audioEngine.playEffect(this.jumpAudio, false);
            }
        }
    }

    setAni (anima) {
        if(this.anima == anima) return;

        this.anima = anima;
        this.playerAni.play(anima);
    }
    
    update (dt) {
        let anima = this.anima;
        let scaleX = Math.abs(this.node.scaleX);
        this.node.angle = 0;
        this.timer += dt;

        this.lv = this.node.getComponent(cc.RigidBody).linearVelocity;
        if(this.canMove){
            if(Input[cc.macro.KEY.w] || Input[cc.macro.KEY.up]){
                if (this.canJump) {
                    this.jump();
                    anima = 'jump';
                }
                if(this.node.scaleX == -scaleX) this.node.angle = -15;
                else if(this.node.scaleX == scaleX) this.node.angle = 15;
            }
            else if(Input[cc.macro.KEY.a] || Input[cc.macro.KEY.left]){
                this.sp.x = -1;
                this.node.scaleX = -scaleX;
                anima = 'idle';
            }
            else if(Input[cc.macro.KEY.d] || Input[cc.macro.KEY.right]){
                this.sp.x = 1;
                this.node.scaleX = scaleX;
                anima = 'idle';
            }
            else if(Input[cc.macro.KEY.l]){
                this.sp.x = 0;
                if (this.timer >= this.intervalTime) {
                    this.timer = 0;
                    this.fire();
                }
            }
            else{
                this.sp.x = 0;
                anima = 'stop';
                if(this.node.scaleX == -scaleX) this.node.angle = -15;
                else if(this.node.scaleX == scaleX) this.node.angle= 15;
            }
        }
        
        if(this.sp.x) this.lv.x = this.sp.x * this._speed;
        else this.lv.x = 0;

        this.node.getComponent(cc.RigidBody).linearVelocity = this.lv;

        if(anima) this.setAni(anima);
    }

    fire() {
        const bullet = cc.instantiate(this.bulletPrefab); // 从预制资源中实例化子弹节点
        const startPos = this.node.getPosition();
        bullet.setPosition(startPos);
        this.node.parent.addChild(bullet);

        cc.audioEngine.playEffect(this.attackAudio, false);

        let moveDir = null;

        if(this.node.scaleX > 0)
            moveDir = cc.moveBy(2, cc.v2(400, 0));
        else
            moveDir = cc.moveBy(2, cc.v2(-400, 0));

        bullet.runAction(
            cc.sequence(
                moveDir, // 子弹向右移动
                cc.callFunc(() => {
                    bullet.removeFromParent(); // 子弹移动结束后从父节点移除
                })
            )
        );
    }

    blacktranfer(){
        this.blacktran.active = true;
        cc.audioEngine.playEffect(this.transferAudio, false);
        this.scheduleOnce(() => {
            this.blacktran.active = false;
        }, 1);
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        console.log("dead!");

        // 判断碰撞的对象是否为敌人
        if (other.node.group === 'king') {
            this.node.setPosition(390, -150);
            cc.audioEngine.playEffect(this.deadAudio, false);
            this.canMove = false;
            this.scheduleOnce(() => {
                this.canMove = true;
            }, 0.5);
        }
        else if(other.node.group === 'blackhole'){
            this.node.setPosition(390, -150);
            this.blacktranfer();
        }
        else if(other.node.group === 'magic1'){
            this.node.setPosition(-327, -76);
            cc.audioEngine.playEffect(this.transferAudio, false);
        }
        else if(other.node.group === 'magic2'){
            this.node.setPosition(336, 92);
            cc.audioEngine.playEffect(this.transferAudio, false);
        }
        else if(other.node.group === 'magic3'){
            this.node.setPosition(242, -200);
            cc.audioEngine.playEffect(this.transferAudio, false);
        }
        else if(other.node.group === 'magic4'){
            this.node.setPosition(-393, -200);
            cc.audioEngine.playEffect(this.transferAudio, false);
        }
        else if(other.node.group === 'magic5'){
            this.node.setPosition(387, -100);
            cc.audioEngine.playEffect(this.transferAudio, false);
        }
    }
}