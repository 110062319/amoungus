const {ccclass, property} = cc._decorator;

@ccclass
export default class UFO extends cc.Component {

    @property()
    ufoSpeed: number = 100;

    @property(cc.Node)
    space: cc.Node = null;

    @property()
    moveDirLR: number = 0;

    @property()
    moveDirUD: number = 0;

    @property(cc.Node)
    UFO: cc.Node = null;

    @property(cc.Node)
    Meteor1: cc.Node = null;

    @property(cc.Node)
    Meteor2: cc.Node = null;

    @property(cc.Node)
    Meteor3: cc.Node = null;

    @property(cc.Node)
    Meteor4: cc.Node = null;

    @property(cc.Node)
    Meteor5: cc.Node = null;

    private physicManager: cc.PhysicsManager = null;

    private aDown: boolean = false;

    private dDown: boolean = false;

    private wDown: boolean = false;

    private sDown: boolean = false;

    onLoad(){
        this.physicManager = cc.director.getPhysicsManager();
        this.physicManager.enabled = true;
        this.physicManager.gravity = cc.v2 (0, 0);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start(){

    }

    update(dt){
        this.UFO.x += this.ufoSpeed * this.moveDirLR * dt;
        this.UFO.y += this.ufoSpeed * this.moveDirUD * dt;

        this.updateMeteorPosition();
    }

    onKeyDown(event){
        switch(event.keyCode){
            case cc.macro.KEY.a:
                this.aDown = true;
                this.ufoMove(-1, this.moveDirUD);
                break;
            case cc.macro.KEY.d:
                this.dDown = true;
                this.ufoMove(1, this.moveDirUD);
                break;
            case cc.macro.KEY.w:
                this.wDown = true;
                this.ufoMove(this.moveDirLR, 1);
                break;
            case cc.macro.KEY.s:
                this.sDown = true;
                this.ufoMove(this.moveDirLR, -1);
                break;
        }
    }

    onKeyUp(event){
        switch(event.keyCode){
            case cc.macro.KEY.a:
                this.aDown = false;
                if(this.dDown)
                    this.ufoMove(1, this.moveDirUD);
                else
                    this.ufoMove(0, this.moveDirUD);
                break;
            case cc.macro.KEY.d:
                this.dDown = false;
                if(this.aDown)
                    this.ufoMove(-1, this.moveDirUD);
                else
                    this.ufoMove(0, this.moveDirUD);
                break;
            case cc.macro.KEY.w:
                this.wDown = false;
                if(this.sDown)
                    this.ufoMove(this.moveDirLR, -1);
                else
                    this.ufoMove(this.moveDirLR, 0);
                break;
            case cc.macro.KEY.s:
                this.sDown = false;
                if(this.wDown)
                    this.ufoMove(this.moveDirLR, 1);
                else
                    this.ufoMove(this.moveDirLR, 0);
                break;
        }
    }

    updateMeteorPosition(){
        if(this.Meteor1.x + (this.Meteor1.width/2) <= this.space.x-480 || this.Meteor1.x - (this.Meteor1.width/2) >= this.space.x+480 || this.Meteor1.y + (this.Meteor1.height/2) <= this.space.y-270 || this.Meteor1.y - (this.Meteor1.height/2) >= this.space.y+270){
            if(this.Meteor2.x + (this.Meteor2.width/2) <= this.space.x-480 || this.Meteor2.x - (this.Meteor2.width/2) >= this.space.x+480 || this.Meteor2.y + (this.Meteor2.height/2) <= this.space.y-270 || this.Meteor2.y - (this.Meteor2.height/2) >= this.space.y+270){
                if(this.Meteor3.x + (this.Meteor3.width/2) <= this.space.x-480 || this.Meteor3.x - (this.Meteor3.width/2) >= this.space.x+480 || this.Meteor3.y + (this.Meteor3.height/2) <= this.space.y-270 || this.Meteor3.y - (this.Meteor3.height/2) >= this.space.y+270){
                    if(this.Meteor4.x + (this.Meteor4.width/2) <= this.space.x-480 || this.Meteor4.x - (this.Meteor4.width/2) >= this.space.x+480 || this.Meteor4.y + (this.Meteor4.height/2) <= this.space.y-270 || this.Meteor4.y - (this.Meteor4.height/2) >= this.space.y+270){
                        if(this.Meteor5.x + (this.Meteor5.width/2) <= this.space.x-480 || this.Meteor5.x - (this.Meteor5.width/2) >= this.space.x+480 || this.Meteor5.y + (this.Meteor5.height/2) <= this.space.y-270 || this.Meteor5.y - (this.Meteor5.height/2) >= this.space.y+270){
                            this.scheduleOnce(() => {
                                cc.log("win");
                                cc.find("Canvas/T9/Task9").active = false;
                                //cc.director.loadScene("default_end");
                            });
                        }
                    }
                }
            }
        }
    }
    
    ufoMove(moveDirLR: number, moveDirUD: number){
        this.moveDirLR = moveDirLR;
        this.moveDirUD = moveDirUD;
    }
}
