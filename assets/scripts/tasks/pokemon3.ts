const { ccclass, property } = cc._decorator;
@ccclass
export default class pokemon3 extends cc.Component {
    @property(cc.Button)
    b1: cc.Button = null;
    @property(cc.Button)
    b2: cc.Button = null;
    @property(cc.Button)
    b3: cc.Button = null;
    @property(cc.Button)
    b4: cc.Button = null;
    @property(cc.Button)
    b5: cc.Button = null;
    @property(cc.Button)
    b6: cc.Button = null;
    @property(cc.SpriteFrame)
    rightFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    wrongFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    originalFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    winFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    loseFrame: cc.SpriteFrame = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    openSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    wrongSE: cc.AudioClip = null;

    begin: number;

    start() {
        this.begin = 0;
        this.playBGM();
    }
    onLoad() {
        this.b1.node.on(cc.Node.EventType.TOUCH_END, this.b1Click, this);
        this.b2.node.on(cc.Node.EventType.TOUCH_END, this.b2Click, this);
        this.b3.node.on(cc.Node.EventType.TOUCH_END, this.b3Click, this);
        this.b4.node.on(cc.Node.EventType.TOUCH_END, this.b4Click, this);
        this.b5.node.on(cc.Node.EventType.TOUCH_END, this.b5Click, this);
        this.b6.node.on(cc.Node.EventType.TOUCH_END, this.b6Click, this);
        this.scheduleOnce(this.popUp, 1.5);
    }
    popUp() {
        cc.audioEngine.playEffect(this.openSE, false);
        this.b1.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
        this.b2.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
        this.b3.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
        this.b4.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
        this.b5.getComponent(cc.Sprite).spriteFrame = this.rightFrame;
        this.b6.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
        this.scheduleOnce(this.popDown, 0.2);
    }
    popDown() {
        this.b1.getComponent(cc.Sprite).spriteFrame = this.originalFrame;
        this.b2.getComponent(cc.Sprite).spriteFrame = this.originalFrame;
        this.b3.getComponent(cc.Sprite).spriteFrame = this.originalFrame;
        this.b4.getComponent(cc.Sprite).spriteFrame = this.originalFrame;
        this.b5.getComponent(cc.Sprite).spriteFrame = this.originalFrame;
        this.b6.getComponent(cc.Sprite).spriteFrame = this.originalFrame;
        this.begin = 1;
    }
    b1Click() {
        if (this.begin) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.b1.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
            this.scheduleOnce(this.lose, 0.5);
        }
    }
    b2Click() {
        if (this.begin) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.b2.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
            this.scheduleOnce(this.lose, 0.5);
        }
    }
    b3Click() {
        if (this.begin) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.b3.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
            this.scheduleOnce(this.lose, 0.5);
        }
    }
    b4Click() {
        if (this.begin) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.b4.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
            this.scheduleOnce(this.lose, 0.5);
        }
    }
    b5Click() {
        if (this.begin) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.rightSE, false);
            this.b5.getComponent(cc.Sprite).spriteFrame = this.rightFrame;
            this.scheduleOnce(this.win, 0.5);
            var GameCond = firebase.database().ref('GameCondition');
            GameCond.once('value', (snpashot) => {
                var currentVal = snpashot.val().Task_completed;
                var NewVal = currentVal + 1;
                GameCond.update({
                    Task_completed: NewVal
                })
            })
        }
    }
    b6Click() {
        if (this.begin) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.b6.getComponent(cc.Sprite).spriteFrame = this.wrongFrame;
            this.scheduleOnce(this.lose, 0.5);
        }
    }
    win() {
        this.b1.node.destroy();
        this.b2.node.destroy();
        this.b3.node.destroy();
        this.b4.node.destroy();
        this.b5.node.destroy();
        this.b6.node.destroy();
        this.node.getComponent(cc.Sprite).spriteFrame = this.winFrame;
        this.scheduleOnce(this.change, 2);
    }
    lose() {
        this.b1.node.destroy();
        this.b2.node.destroy();
        this.b3.node.destroy();
        this.b4.node.destroy();
        this.b5.node.destroy();
        this.b6.node.destroy();
        this.node.getComponent(cc.Sprite).spriteFrame = this.loseFrame;
        this.scheduleOnce(this.change, 2);
    }
    change() {
        cc.director.loadScene("Skeld");
    }
    playBGM() {
        cc.audioEngine.playMusic(this.bgm, true);
    }
}

