const {ccclass, property} = cc._decorator;
@ccclass
export default class two_player_die extends cc.Component {
    @property(cc.AudioClip)
    loseSE: cc.AudioClip = null;

    onLoad(){
        cc.director.getPhysicsManager().enabled=true;
    }
    onBeginContact(contact,self,other){
        if(other.node.name=="missile0"||other.node.name=="missile1"||other.node.name=="missile2"||other.node.name=="missile3"){
            cc.audioEngine.playEffect(this.loseSE, false);
            cc.find("Canvas/T8/Task8").active = false;
            //cc.director.loadScene("default_end");
        } 
        if(other.node.name=="missile4"||other.node.name=="missile5"||other.node.name=="missile6"||other.node.name=="missile7"){
            cc.audioEngine.playEffect(this.loseSE, false);
            cc.find("Canvas/T8/Task8").active = false;
            //cc.director.loadScene("default_end");
        } 
        if(other.node.name=="missile8"||other.node.name=="missile9"||other.node.name=="missile10"||other.node.name=="missile11"){
            cc.audioEngine.playEffect(this.loseSE, false);
            cc.find("Canvas/T8/Task8").active = false;
            //cc.director.loadScene("default_end");
        } 
        if(other.node.name=="missile12"||other.node.name=="missile13"||other.node.name=="missile14"||other.node.name=="missile15"){
            cc.audioEngine.playEffect(this.loseSE, false);
            cc.find("Canvas/T8/Task8").active = false;
            //cc.director.loadScene("default_end");
        } 
    }
}
