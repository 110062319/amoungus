const { ccclass, property } = cc._decorator;
declare const firebase: any;
@ccclass
export default class pressSameButton extends cc.Component {
    @property(cc.Button)
    lBlue: cc.Button = null;
    @property(cc.Button)
    lRed: cc.Button = null;
    @property(cc.Button)
    lYellow: cc.Button = null;
    @property(cc.Button)
    lGreen: cc.Button = null;
    @property(cc.Button)
    rBlue: cc.Button = null;
    @property(cc.Button)
    rRed: cc.Button = null;
    @property(cc.Button)
    rYellow: cc.Button = null;
    @property(cc.Button)
    rGreen: cc.Button = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    leftSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightRightSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightWrongSE: cc.AudioClip = null;

    lbc: number;
    lrc: number;
    lyc: number;
    lgc: number;
    pressed: number;

    start() {
        this.lbc = 1;
        this.lrc = 0;
        this.lyc = 0;
        this.lgc = 0;
        this.pressed = 0;
        this.playBGM();
    }
    onLoad() {
        this.lBlue.node.on(cc.Node.EventType.TOUCH_END, this.lBlueClick, this);
        this.lRed.node.on(cc.Node.EventType.TOUCH_END, this.lRedClick, this);
        this.lYellow.node.on(cc.Node.EventType.TOUCH_END, this.lYellowClick, this);
        this.lGreen.node.on(cc.Node.EventType.TOUCH_END, this.lGreenClick, this);
        this.rBlue.node.on(cc.Node.EventType.TOUCH_END, this.rBlueClick, this);
        this.rRed.node.on(cc.Node.EventType.TOUCH_END, this.rRedClick, this);
        this.rYellow.node.on(cc.Node.EventType.TOUCH_END, this.rYellowClick, this);
        this.rGreen.node.on(cc.Node.EventType.TOUCH_END, this.rGreenClick, this);
    }
    update(dt) {
        if (this.pressed == 4) {
            cc.audioEngine.stopMusic();
            cc.find("Canvas/T1/Task1").active = false;
            var GameCond = firebase.database().ref('GameCondition');
            GameCond.once('value', (snpashot) => {
                var currentVal = snpashot.val().Task_completed;
                var NewVal = currentVal + 1;
                GameCond.update({
                    Task_completed: NewVal
                })
            })
        }
    }
    lBlueClick() {
        this.lbc = 1;
        this.lrc = 0;
        this.lyc = 0;
        this.lgc = 0;
        cc.audioEngine.playEffect(this.leftSE, false);
    }
    lRedClick() {
        this.lbc = 0;
        this.lrc = 1;
        this.lyc = 0;
        this.lgc = 0;
        cc.audioEngine.playEffect(this.leftSE, false);
    }
    lYellowClick() {
        this.lbc = 0;
        this.lrc = 0;
        this.lyc = 1;
        this.lgc = 0;
        cc.audioEngine.playEffect(this.leftSE, false);
    }
    lGreenClick() {
        this.lbc = 0;
        this.lrc = 0;
        this.lyc = 0;
        this.lgc = 1;
        cc.audioEngine.playEffect(this.leftSE, false);
    }
    rBlueClick() {
        if (this.lbc == 1) {
            cc.audioEngine.playEffect(this.rightRightSE, false);
            this.lBlue.node.destroy();
            this.rBlue.node.destroy();
            this.pressed++;
        }
        else {
            cc.audioEngine.playEffect(this.rightWrongSE, false);
            this.lbc = 0;
            this.lrc = 0;
            this.lyc = 0;
            this.lgc = 0;
        }
    }
    rRedClick() {
        if (this.lrc == 1) {
            cc.audioEngine.playEffect(this.rightRightSE, false);
            this.lRed.node.destroy();
            this.rRed.node.destroy();
            this.pressed++;
        }
        else {
            cc.audioEngine.playEffect(this.rightWrongSE, false);
            this.lbc = 0;
            this.lrc = 0;
            this.lyc = 0;
            this.lgc = 0;
        }
    }
    rYellowClick() {
        if (this.lyc == 1) {
            cc.audioEngine.playEffect(this.rightRightSE, false);
            this.lYellow.node.destroy();
            this.rYellow.node.destroy();
            this.pressed++;
        }
        else {
            cc.audioEngine.playEffect(this.rightWrongSE, false);
            this.lbc = 0;
            this.lrc = 0;
            this.lyc = 0;
            this.lgc = 0;
        }
    }
    rGreenClick() {
        if (this.lgc == 1) {
            cc.audioEngine.playEffect(this.rightRightSE, false);
            this.lGreen.node.destroy();
            this.rGreen.node.destroy();
            this.pressed++;
        }
        else {
            cc.audioEngine.playEffect(this.rightWrongSE, false);
            this.lbc = 0;
            this.lrc = 0;
            this.lyc = 0;
            this.lgc = 0;
        }
    }
    playBGM() {
        cc.audioEngine.playMusic(this.bgm, true);
    }
}
