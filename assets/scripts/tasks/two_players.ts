const { ccclass, property } = cc._decorator;
declare const firebase: any;
@ccclass
export default class two_players extends cc.Component {
    @property(cc.Node)
    p1: cc.Node = null;
    @property(cc.Node)
    p2: cc.Node = null;
    @property(cc.Node)
    m0: cc.Node = null;
    @property(cc.Node)
    m1: cc.Node = null;
    @property(cc.Node)
    m2: cc.Node = null;
    @property(cc.Node)
    m3: cc.Node = null;
    @property(cc.Node)
    m4: cc.Node = null;
    @property(cc.Node)
    m5: cc.Node = null;
    @property(cc.Node)
    m6: cc.Node = null;
    @property(cc.Node)
    m7: cc.Node = null;
    @property(cc.Node)
    m8: cc.Node = null;
    @property(cc.Node)
    m9: cc.Node = null;
    @property(cc.Node)
    m10: cc.Node = null;
    @property(cc.Node)
    m11: cc.Node = null;
    @property(cc.Node)
    m12: cc.Node = null;
    @property(cc.Node)
    m13: cc.Node = null;
    @property(cc.Node)
    m14: cc.Node = null;
    @property(cc.Node)
    m15: cc.Node = null;
    @property(cc.Node)
    m16: cc.Node = null;
    @property(cc.Node)
    m17: cc.Node = null;
    @property(cc.Node)
    m18: cc.Node = null;
    @property(cc.Node)
    m19: cc.Node = null;
    @property(cc.Node)
    m20: cc.Node = null;
    @property(cc.Node)
    m21: cc.Node = null;
    @property(cc.Node)
    m22: cc.Node = null;
    @property(cc.Node)
    m23: cc.Node = null;
    @property(cc.AudioClip)
    launchSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    winSE: cc.AudioClip = null;

    private p1IsMovingLeft: boolean = false;
    private p1IsMovingRight: boolean = false;
    private p1IsMovingUp: boolean = false;
    private p1IsMovingDown: boolean = false;
    private p2IsMovingLeft: boolean = false;
    private p2IsMovingRight: boolean = false;
    private p2IsMovingUp: boolean = false;
    private p2IsMovingDown: boolean = false;
    private m0Launched: boolean = true;
    private m1Launched: boolean = false;
    private m2Launched: boolean = false;
    private m3Launched: boolean = true;
    private m4Launched: boolean = true;
    private m5Launched: boolean = true;
    private m6Launched: boolean = true;
    private m7Launched: boolean = true;
    private m8Launched: boolean = true;
    private m9Launched: boolean = true;
    private m10Launched: boolean = true;
    private m11Launched: boolean = true;
    private m12Launched: boolean = true;
    private m13Launched: boolean = true;
    private m14Launched: boolean = true;
    private m15Launched: boolean = true;
    private m16Launched: boolean = true;
    private m17Launched: boolean = true;
    private m18Launched: boolean = true;
    private m19Launched: boolean = true;
    private m20Launched: boolean = true;
    private m21Launched: boolean = true;
    private m22Launched: boolean = true;
    private m23Launched: boolean = true;
    moveSpeed: number;
    randNum: number;
    count: number;
    begin: number;

    start() {
        this.moveSpeed = 200;
        this.randNum = Math.floor(Math.random() * 24);
        this.count = 0;
        this.begin = 0;
        this.scheduleOnce(this.countdown, 3);
    }
    countdown() {
        this.begin++;
    }
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    update(dt) {
        if (this.count == 24) {
            this.scheduleOnce(this.changeScene, 8);
        }
        this.playerMovement(dt);
        if (this.begin) this.launchMissile(dt);
    }
    changeScene() {
        cc.audioEngine.playEffect(this.winSE, false);
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.once('value', (snpashot) => {
            var currentVal = snpashot.val().Task_completed;
            var NewVal = currentVal + 1;
            GameCond.update({
                Task_completed: NewVal
            })
        })
        cc.find("Canvas/T8").active = false;
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.once('value', (snpashot) => {
            var currentVal = snpashot.val().Task_completed;
            var NewVal = currentVal + 1;
            GameCond.update({
                Task_completed: NewVal
            })
        })
        //cc.director.loadScene("default_end");
    }
    playerMovement(dt) {
        if (this.p1IsMovingLeft) {
            this.p1.x -= this.moveSpeed * dt;
        }
        if (this.p1IsMovingRight) {
            this.p1.x += this.moveSpeed * dt;
        }
        if (this.p1IsMovingUp) {
            this.p1.y += this.moveSpeed * dt;
        }
        if (this.p1IsMovingDown) {
            this.p1.y -= this.moveSpeed * dt;
        }
        if (this.p2IsMovingLeft) {
            this.p2.x -= this.moveSpeed * dt;
        }
        if (this.p2IsMovingRight) {
            this.p2.x += this.moveSpeed * dt;
        }
        if (this.p2IsMovingUp) {
            this.p2.y += this.moveSpeed * dt;
        }
        if (this.p2IsMovingDown) {
            this.p2.y -= this.moveSpeed * dt;
        }
    }
    launchMissile(dt) {
        if (this.randNum == 0 && this.m0Launched) {
            this.m0Launched = false;
            this.m0.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 1 && this.m1Launched) {
            this.m1Launched = false;
            this.m1.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 2 && this.m2Launched) {
            this.m2Launched = false;
            this.m2.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 3 && this.m3Launched) {
            this.m3Launched = false;
            this.m3.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 4 && this.m4Launched) {
            this.m4Launched = false;
            this.m4.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 5 && this.m5Launched) {
            this.m5Launched = false;
            this.m5.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 6 && this.m6Launched) {
            this.m6Launched = false;
            this.m6.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 7 && this.m7Launched) {
            this.m7Launched = false;
            this.m7.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 8 && this.m8Launched) {
            this.m8Launched = false;
            this.m8.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 9 && this.m9Launched) {
            this.m9Launched = false;
            this.m9.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 10 && this.m10Launched) {
            this.m10Launched = false;
            this.m10.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 11 && this.m11Launched) {
            this.m11Launched = false;
            this.m11.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 12 && this.m12Launched) {
            this.m12Launched = false;
            this.m12.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 13 && this.m13Launched) {
            this.m13Launched = false;
            this.m13.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 14 && this.m14Launched) {
            this.m14Launched = false;
            this.m14.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 15 && this.m15Launched) {
            this.m15Launched = false;
            this.m15.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 16 && this.m16Launched) {
            this.m16Launched = false;
            this.m16.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 17 && this.m17Launched) {
            this.m17Launched = false;
            this.m17.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 18 && this.m18Launched) {
            this.m18Launched = false;
            this.m18.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 19 && this.m19Launched) {
            this.m19Launched = false;
            this.m19.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 20 && this.m20Launched) {
            this.m20Launched = false;
            this.m20.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 21 && this.m21Launched) {
            this.m21Launched = false;
            this.m21.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 22 && this.m22Launched) {
            this.m22Launched = false;
            this.m22.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
        if (this.randNum == 23 && this.m23Launched) {
            this.m23Launched = false;
            this.m23.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -200);
            cc.audioEngine.playEffect(this.launchSE, false);
            this.count++;
            this.scheduleOnce(this.add, 0.4);
        }
    }
    add() {
        this.randNum = (this.randNum + 1) % 24;
    }
    onKeyDown(e) {
        if (e.keyCode === cc.macro.KEY.a) {
            this.p1IsMovingLeft = true;
        }
        if (e.keyCode === cc.macro.KEY.d) {
            this.p1IsMovingRight = true;
        }
        if (e.keyCode === cc.macro.KEY.w) {
            this.p1IsMovingUp = true;
        }
        if (e.keyCode === cc.macro.KEY.s) {
            this.p1IsMovingDown = true;
        }
        if (e.keyCode === cc.macro.KEY.j) {
            this.p2IsMovingLeft = true;
        }
        if (e.keyCode === cc.macro.KEY.l) {
            this.p2IsMovingRight = true;
        }
        if (e.keyCode === cc.macro.KEY.i) {
            this.p2IsMovingUp = true;
        }
        if (e.keyCode === cc.macro.KEY.k) {
            this.p2IsMovingDown = true;
        }
    }
    onKeyUp(e) {
        if (e.keyCode === cc.macro.KEY.a) {
            this.p1IsMovingLeft = false;
        }
        if (e.keyCode === cc.macro.KEY.d) {
            this.p1IsMovingRight = false;
        }
        if (e.keyCode === cc.macro.KEY.w) {
            this.p1IsMovingUp = false;
        }
        if (e.keyCode === cc.macro.KEY.s) {
            this.p1IsMovingDown = false;
        }
        if (e.keyCode === cc.macro.KEY.j) {
            this.p2IsMovingLeft = false;
        }
        if (e.keyCode === cc.macro.KEY.l) {
            this.p2IsMovingRight = false;
        }
        if (e.keyCode === cc.macro.KEY.i) {
            this.p2IsMovingUp = false;
        }
        if (e.keyCode === cc.macro.KEY.k) {
            this.p2IsMovingDown = false;
        }
    }
}
