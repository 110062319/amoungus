const { ccclass, property } = cc._decorator;
@ccclass
export default class sequence1 extends cc.Component {
    @property(cc.Node)
    u1: cc.Node = null;
    @property(cc.Node)
    u2: cc.Node = null;
    @property(cc.Node)
    u3: cc.Node = null;
    @property(cc.Node)
    u4: cc.Node = null;
    @property(cc.Node)
    u5: cc.Node = null;
    @property(cc.Node)
    u6: cc.Node = null;
    @property(cc.Button)
    red: cc.Button = null;
    @property(cc.Button)
    green: cc.Button = null;
    @property(cc.Button)
    blue: cc.Button = null;
    @property(cc.Button)
    yellow: cc.Button = null;
    @property(cc.Button)
    purple: cc.Button = null;
    @property(cc.Button)
    black: cc.Button = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    wrongSE: cc.AudioClip = null;

    begin: number;
    pressed: number;

    start() {
        this.begin = 0;
        this.pressed = 0;
        this.playBGM();
    }
    onLoad() {
        this.red.node.on(cc.Node.EventType.TOUCH_END, this.redClick, this);
        this.green.node.on(cc.Node.EventType.TOUCH_END, this.greenClick, this);
        this.blue.node.on(cc.Node.EventType.TOUCH_END, this.blueClick, this);
        this.yellow.node.on(cc.Node.EventType.TOUCH_END, this.yellowClick, this);
        this.purple.node.on(cc.Node.EventType.TOUCH_END, this.purpleClick, this);
        this.black.node.on(cc.Node.EventType.TOUCH_END, this.blackClick, this);
        this.scheduleOnce(this.click, 5);
    }
    update(dt) {
        if (this.pressed == 6) {
            cc.audioEngine.stopMusic();
            //cc.director.loadScene("Skeld");
            cc.find("Canvas/T6/Task6").active = false;
            var GameCond = firebase.database().ref('GameCondition');
            GameCond.once('value', (snpashot) => {
                var currentVal = snpashot.val().Task_completed;
                var NewVal = currentVal + 1;
                GameCond.update({
                    Task_completed: NewVal
                })
            })
        }
    }
    click() {
        this.u1.destroy();
        this.u2.destroy();
        this.u3.destroy();
        this.u4.destroy();
        this.u5.destroy();
        this.u6.destroy();
        this.begin = 1;
    }
    redClick() {
        if (this.begin && this.pressed == 4) {
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else if (this.begin) {
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed = 0;
        }
    }
    greenClick() {
        if (this.begin && this.pressed == 2) {
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else if (this.begin) {
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed = 0;
        }
    }
    blueClick() {
        if (this.begin && this.pressed == 1) {
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else if (this.begin) {
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed = 0;
        }
    }
    yellowClick() {
        if (this.begin && this.pressed == 3) {
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else if (this.begin) {
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed = 0;
        }
    }
    purpleClick() {
        if (this.begin && this.pressed == 5) {
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else if (this.begin) {
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed = 0;
        }
    }
    blackClick() {
        if (this.begin && this.pressed == 0) {
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else if (this.begin) {
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed = 0;
        }
    }

    playBGM() {
        cc.audioEngine.playMusic(this.bgm, true);
    }
}
