import UFO from "../UFO";const {ccclass, property} = cc._decorator;

@ccclass
export default class Meteor extends cc.Component {

    @property(UFO)
    UFO: UFO = null;

    private physicManager: cc.PhysicsManager = null;

    onLoad(){
        this.physicManager = cc.director.getPhysicsManager();
        this.physicManager.enabled = true;
        this.physicManager.gravity = cc.v2 (0, 0);
    }

    start(){}

    update(dt){}

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.node.name == "UFO"){
            const meteorRigidBody = this.node.getComponent(cc.RigidBody);
            if(meteorRigidBody){
                const pushForce = cc.v2(this.UFO.moveDirLR * 12000, this.UFO.moveDirUD * 12000);
                meteorRigidBody.applyForceToCenter(pushForce, true);
            }
        }
    }
}
