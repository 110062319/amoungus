const { ccclass, property } = cc._decorator;
declare const firebase: any;
@ccclass
export default class ai extends cc.Component {
    @property(cc.Node)
    enemy: cc.Node = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    winSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    loseSE: cc.AudioClip = null;

    private isMovingLeft: boolean = false;
    private isMovingRight: boolean = false;
    private isMovingUp: boolean = false;
    private isMovingDown: boolean = false;
    randNum: number;
    pMoveSpeed: number;
    eMoveSpeed: number;
    begin: number;

    start() {
        cc.audioEngine.playMusic(this.bgm, true);
        this.pMoveSpeed = 200;
        this.eMoveSpeed = 100;
        this.randNum = Math.floor(Math.random() * 441) + 50;
        this.node.y = this.randNum;
        this.enemy.y = this.randNum;
        this.begin = 0;
        this.scheduleOnce(this.countdown1, 3);
        this.scheduleOnce(this.countdown2, 18);
    }
    countdown1() {
        this.begin++;
    }
    countdown2() {
        this.begin++;
    }
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    update(dt) {
        if (this.begin == 2) {
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.winSE, false);
            var GameCond = firebase.database().ref('GameCondition');
            GameCond.once('value', (snpashot) => {
                var currentVal = snpashot.val().Task_completed;
                var NewVal = currentVal + 1;
                GameCond.update({
                    Task_completed: NewVal
                })
            })
            cc.director.loadScene("default_end");
        }
        this.playerMovement(dt);
        this.enemyMovement(dt);
    }
    playerMovement(dt) {
        if (this.isMovingLeft) {
            this.node.x -= this.pMoveSpeed * dt;
        }
        if (this.isMovingRight) {
            this.node.x += this.pMoveSpeed * dt;
        }
        if (this.isMovingUp) {
            this.node.y += this.pMoveSpeed * dt;
        }
        if (this.isMovingDown) {
            this.node.y -= this.pMoveSpeed * dt;
        }
    }
    enemyMovement(dt) {
        if (this.begin) {
            const direction = this.node.position.sub(this.enemy.position);
            const distance = direction.mag();
            const normalizedDirection = direction.normalize();
            const speed = this.eMoveSpeed * dt;
            const movement = normalizedDirection.mul(speed);
            if (distance > speed) {
                this.enemy.position = this.enemy.position.add(movement);
            } else {
                this.enemy.position = this.node.position.clone();
            }
        }
    }
    onKeyDown(e) {
        if (e.keyCode === cc.macro.KEY.left) {
            this.isMovingLeft = true;
        }
        if (e.keyCode === cc.macro.KEY.right) {
            this.isMovingRight = true;
        }
        if (e.keyCode === cc.macro.KEY.up) {
            this.isMovingUp = true;
        }
        if (e.keyCode === cc.macro.KEY.down) {
            this.isMovingDown = true;
        }
    }
    onKeyUp(e) {
        if (e.keyCode === cc.macro.KEY.left) {
            this.isMovingLeft = false;
        }
        if (e.keyCode === cc.macro.KEY.right) {
            this.isMovingRight = false;
        }
        if (e.keyCode === cc.macro.KEY.up) {
            this.isMovingUp = false;
        }
        if (e.keyCode === cc.macro.KEY.down) {
            this.isMovingDown = false;
        }
    }
    onBeginContact(contact, self, other) {
        if (other.node.name == "enemy") {
            cc.audioEngine.stopMusic()
            cc.audioEngine.playEffect(this.loseSE, false);
            cc.director.loadScene("default_end");
        }
    }
}
