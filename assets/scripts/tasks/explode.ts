const { ccclass, property } = cc._decorator;
declare const firebase: any;
@ccclass
export default class explode extends cc.Component {
    @property(cc.Node)
    earth: cc.Node = null;
    @property(cc.Button)
    bang: cc.Button = null;
    @property(cc.SpriteFrame)
    normalFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    rightFrame: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    explodeFrame: cc.SpriteFrame = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    explodeSE: cc.AudioClip = null;

    start() {
        this.playBGM();
    }
    onLoad() {
        this.bang.node.on(cc.Node.EventType.TOUCH_END, this.bangClick, this);
        this.scheduleOnce(this.right, 10);
    }
    right() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playEffect(this.rightSE, false);
        this.earth.getComponent(cc.Sprite).spriteFrame = this.rightFrame;
        this.scheduleOnce(this.win, 1.5);
    }
    bangClick() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playEffect(this.explodeSE, false);
        this.earth.getComponent(cc.Sprite).spriteFrame = this.explodeFrame;
        this.scheduleOnce(this.change, 1.5);
    }
    change() {
        this.earth.getComponent(cc.Sprite).spriteFrame = this.normalFrame;
        this.playBGM();
    }
    win() {
        var GameCond = firebase.database().ref('GameCondition');
        GameCond.once('value', (snpashot) => {
            var currentVal = snpashot.val().Task_completed;
            var NewVal = currentVal + 1;
            GameCond.update({
                Task_completed: NewVal
            })
        })
        cc.find("Canvas/T2").active = false;
    }
    playBGM() {
        cc.audioEngine.playMusic(this.bgm, true);
    }
}
