const { ccclass, property } = cc._decorator;
@ccclass
export default class particle extends cc.Component {
    @property(cc.Node)
    r100_0: cc.Node = null;
    @property(cc.Node)
    r100_1: cc.Node = null;
    @property(cc.Node)
    r100_2: cc.Node = null;
    @property(cc.Node)
    r100_3: cc.Node = null;
    @property(cc.Node)
    r100_4: cc.Node = null;
    @property(cc.Node)
    r100_5: cc.Node = null;
    @property(cc.Node)
    r100_6: cc.Node = null;
    @property(cc.Node)
    r100_7: cc.Node = null;
    @property(cc.Node)
    r100_8: cc.Node = null;
    @property(cc.Node)
    r100_9: cc.Node = null;
    @property(cc.Node)
    f100_0: cc.Node = null;
    @property(cc.Node)
    f100_1: cc.Node = null;
    @property(cc.Node)
    f100_2: cc.Node = null;
    @property(cc.Node)
    f100_3: cc.Node = null;
    @property(cc.Node)
    f100_4: cc.Node = null;
    @property(cc.Node)
    f100_5: cc.Node = null;
    @property(cc.Node)
    f100_6: cc.Node = null;
    @property(cc.Node)
    f100_7: cc.Node = null;
    @property(cc.Node)
    f100_8: cc.Node = null;
    @property(cc.Node)
    f100_9: cc.Node = null;
    @property(cc.Node)
    r500_0: cc.Node = null;
    @property(cc.Node)
    r500_1: cc.Node = null;
    @property(cc.Node)
    r500_2: cc.Node = null;
    @property(cc.Node)
    r500_3: cc.Node = null;
    @property(cc.Node)
    r500_4: cc.Node = null;
    @property(cc.Node)
    r500_5: cc.Node = null;
    @property(cc.Node)
    r500_6: cc.Node = null;
    @property(cc.Node)
    r500_7: cc.Node = null;
    @property(cc.Node)
    r500_8: cc.Node = null;
    @property(cc.Node)
    r500_9: cc.Node = null;
    @property(cc.Node)
    f500_0: cc.Node = null;
    @property(cc.Node)
    f500_1: cc.Node = null;
    @property(cc.Node)
    f500_2: cc.Node = null;
    @property(cc.Node)
    f500_3: cc.Node = null;
    @property(cc.Node)
    f500_4: cc.Node = null;
    @property(cc.Node)
    f500_5: cc.Node = null;
    @property(cc.Node)
    f500_6: cc.Node = null;
    @property(cc.Node)
    f500_7: cc.Node = null;
    @property(cc.Node)
    f500_8: cc.Node = null;
    @property(cc.Node)
    f500_9: cc.Node = null;
    @property(cc.Node)
    r1000_0: cc.Node = null;
    @property(cc.Node)
    r1000_1: cc.Node = null;
    @property(cc.Node)
    r1000_2: cc.Node = null;
    @property(cc.Node)
    r1000_3: cc.Node = null;
    @property(cc.Node)
    r1000_4: cc.Node = null;
    @property(cc.Node)
    r1000_5: cc.Node = null;
    @property(cc.Node)
    r1000_6: cc.Node = null;
    @property(cc.Node)
    r1000_7: cc.Node = null;
    @property(cc.Node)
    r1000_8: cc.Node = null;
    @property(cc.Node)
    r1000_9: cc.Node = null;
    @property(cc.Node)
    f1000_0: cc.Node = null;
    @property(cc.Node)
    f1000_1: cc.Node = null;
    @property(cc.Node)
    f1000_2: cc.Node = null;
    @property(cc.Node)
    f1000_3: cc.Node = null;
    @property(cc.Node)
    f1000_4: cc.Node = null;
    @property(cc.Node)
    f1000_5: cc.Node = null;
    @property(cc.Node)
    f1000_6: cc.Node = null;
    @property(cc.Node)
    f1000_7: cc.Node = null;
    @property(cc.Node)
    f1000_8: cc.Node = null;
    @property(cc.Node)
    f1000_9: cc.Node = null;
    @property(cc.Node)
    g0: cc.Node = null;
    @property(cc.Node)
    g1: cc.Node = null;
    @property(cc.Node)
    g2: cc.Node = null;
    @property(cc.Node)
    g3: cc.Node = null;
    @property(cc.Node)
    g4: cc.Node = null;
    @property(cc.Node)
    g5: cc.Node = null;
    @property(cc.Node)
    g6: cc.Node = null;
    @property(cc.Node)
    g7: cc.Node = null;
    @property(cc.Node)
    g8: cc.Node = null;
    @property(cc.Node)
    g9: cc.Node = null;
    @property(cc.Label)
    sLabel: cc.Label = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    wrongSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    winSE: cc.AudioClip = null;

    randNum: number;
    randX: number;
    randY: number;
    count: number;
    goldCount: number;
    nr100_0: number;
    nr100_1: number;
    nr100_2: number;
    nr100_3: number;
    nr100_4: number;
    nr100_5: number;
    nr100_6: number;
    nr100_7: number;
    nr100_8: number;
    nr100_9: number;
    nr500_0: number;
    nr500_1: number;
    nr500_2: number;
    nr500_3: number;
    nr500_4: number;
    nr500_5: number;
    nr500_6: number;
    nr500_7: number;
    nr500_8: number;
    nr500_9: number;
    nr1000_0: number;
    nr1000_1: number;
    nr1000_2: number;
    nr1000_3: number;
    nr1000_4: number;
    nr1000_5: number;
    nr1000_6: number;
    nr1000_7: number;
    nr1000_8: number;
    nr1000_9: number;
    nf100_0: number;
    nf100_1: number;
    nf100_2: number;
    nf100_3: number;
    nf100_4: number;
    nf100_5: number;
    nf100_6: number;
    nf100_7: number;
    nf100_8: number;
    nf100_9: number;
    nf500_0: number;
    nf500_1: number;
    nf500_2: number;
    nf500_3: number;
    nf500_4: number;
    nf500_5: number;
    nf500_6: number;
    nf500_7: number;
    nf500_8: number;
    nf500_9: number;
    nf1000_0: number;
    nf1000_1: number;
    nf1000_2: number;
    nf1000_3: number;
    nf1000_4: number;
    nf1000_5: number;
    nf1000_6: number;
    nf1000_7: number;
    nf1000_8: number;
    nf1000_9: number;
    ng_0: number;
    ng_1: number;
    ng_2: number;
    ng_3: number;
    ng_4: number;
    ng_5: number;
    ng_6: number;
    ng_7: number;
    ng_8: number;
    ng_9: number;
    check: number;
    score: number;

    start() {
        cc.audioEngine.playMusic(this.bgm, true);
        this.randNum = 0;
        this.randX = 0;
        this.randY = 0;
        this.count = 0;
        this.goldCount = 0;
        this.nr100_0 = 0;
        this.nr100_1 = 0;
        this.nr100_2 = 0;
        this.nr100_3 = 0;
        this.nr100_4 = 0;
        this.nr100_5 = 0;
        this.nr100_6 = 0;
        this.nr100_7 = 0;
        this.nr100_8 = 0;
        this.nr100_9 = 0;
        this.nr500_0 = 0;
        this.nr500_1 = 0;
        this.nr500_2 = 0;
        this.nr500_3 = 0;
        this.nr500_4 = 0;
        this.nr500_5 = 0;
        this.nr500_6 = 0;
        this.nr500_7 = 0;
        this.nr500_8 = 0;
        this.nr500_9 = 0;
        this.nr1000_0 = 0;
        this.nr1000_1 = 0;
        this.nr1000_2 = 0;
        this.nr1000_3 = 0;
        this.nr1000_4 = 0;
        this.nr1000_5 = 0;
        this.nr1000_6 = 0;
        this.nr1000_7 = 0;
        this.nr1000_8 = 0;
        this.nr1000_9 = 0;
        this.nf100_0 = 0;
        this.nf100_1 = 0;
        this.nf100_2 = 0;
        this.nf100_3 = 0;
        this.nf100_4 = 0;
        this.nf100_5 = 0;
        this.nf100_6 = 0;
        this.nf100_7 = 0;
        this.nf100_8 = 0;
        this.nf100_9 = 0;
        this.nf500_0 = 0;
        this.nf500_1 = 0;
        this.nf500_2 = 0;
        this.nf500_3 = 0;
        this.nf500_4 = 0;
        this.nf500_5 = 0;
        this.nf500_6 = 0;
        this.nf500_7 = 0;
        this.nf500_8 = 0;
        this.nf500_9 = 0;
        this.nf1000_0 = 0;
        this.nf1000_1 = 0;
        this.nf1000_2 = 0;
        this.nf1000_3 = 0;
        this.nf1000_4 = 0;
        this.nf1000_5 = 0;
        this.nf1000_6 = 0;
        this.nf1000_7 = 0;
        this.nf1000_8 = 0;
        this.nf1000_9 = 0;
        this.ng_0 = 0;
        this.ng_1 = 0;
        this.ng_2 = 0;
        this.ng_3 = 0;
        this.ng_4 = 0;
        this.ng_5 = 0;
        this.ng_6 = 0;
        this.ng_7 = 0;
        this.ng_8 = 0;
        this.ng_9 = 0;
        this.check = 0;
        this.score = 0;
        this.sLabel.string = "0";
        this.schedule(this.cal, 0.5)
    }
    onLoad() {
        this.r100_0.on(cc.Node.EventType.TOUCH_END, this.r100_0Click, this);
        this.r100_1.on(cc.Node.EventType.TOUCH_END, this.r100_1Click, this);
        this.r100_2.on(cc.Node.EventType.TOUCH_END, this.r100_2Click, this);
        this.r100_3.on(cc.Node.EventType.TOUCH_END, this.r100_3Click, this);
        this.r100_4.on(cc.Node.EventType.TOUCH_END, this.r100_4Click, this);
        this.r100_5.on(cc.Node.EventType.TOUCH_END, this.r100_5Click, this);
        this.r100_6.on(cc.Node.EventType.TOUCH_END, this.r100_6Click, this);
        this.r100_7.on(cc.Node.EventType.TOUCH_END, this.r100_7Click, this);
        this.r100_8.on(cc.Node.EventType.TOUCH_END, this.r100_8Click, this);
        this.r100_9.on(cc.Node.EventType.TOUCH_END, this.r100_9Click, this);
        this.r500_0.on(cc.Node.EventType.TOUCH_END, this.r500_0Click, this);
        this.r500_1.on(cc.Node.EventType.TOUCH_END, this.r500_1Click, this);
        this.r500_2.on(cc.Node.EventType.TOUCH_END, this.r500_2Click, this);
        this.r500_3.on(cc.Node.EventType.TOUCH_END, this.r500_3Click, this);
        this.r500_4.on(cc.Node.EventType.TOUCH_END, this.r500_4Click, this);
        this.r500_5.on(cc.Node.EventType.TOUCH_END, this.r500_5Click, this);
        this.r500_6.on(cc.Node.EventType.TOUCH_END, this.r500_6Click, this);
        this.r500_7.on(cc.Node.EventType.TOUCH_END, this.r500_7Click, this);
        this.r500_8.on(cc.Node.EventType.TOUCH_END, this.r500_8Click, this);
        this.r500_9.on(cc.Node.EventType.TOUCH_END, this.r500_9Click, this);
        this.r1000_0.on(cc.Node.EventType.TOUCH_END, this.r1000_0Click, this);
        this.r1000_1.on(cc.Node.EventType.TOUCH_END, this.r1000_1Click, this);
        this.r1000_2.on(cc.Node.EventType.TOUCH_END, this.r1000_2Click, this);
        this.r1000_3.on(cc.Node.EventType.TOUCH_END, this.r1000_3Click, this);
        this.r1000_4.on(cc.Node.EventType.TOUCH_END, this.r1000_4Click, this);
        this.r1000_5.on(cc.Node.EventType.TOUCH_END, this.r1000_5Click, this);
        this.r1000_6.on(cc.Node.EventType.TOUCH_END, this.r1000_6Click, this);
        this.r1000_7.on(cc.Node.EventType.TOUCH_END, this.r1000_7Click, this);
        this.r1000_8.on(cc.Node.EventType.TOUCH_END, this.r1000_8Click, this);
        this.r1000_9.on(cc.Node.EventType.TOUCH_END, this.r1000_9Click, this);
        this.f100_0.on(cc.Node.EventType.TOUCH_END, this.f100_0Click, this);
        this.f100_1.on(cc.Node.EventType.TOUCH_END, this.f100_1Click, this);
        this.f100_2.on(cc.Node.EventType.TOUCH_END, this.f100_2Click, this);
        this.f100_3.on(cc.Node.EventType.TOUCH_END, this.f100_3Click, this);
        this.f100_4.on(cc.Node.EventType.TOUCH_END, this.f100_4Click, this);
        this.f100_5.on(cc.Node.EventType.TOUCH_END, this.f100_5Click, this);
        this.f100_6.on(cc.Node.EventType.TOUCH_END, this.f100_6Click, this);
        this.f100_7.on(cc.Node.EventType.TOUCH_END, this.f100_7Click, this);
        this.f100_8.on(cc.Node.EventType.TOUCH_END, this.f100_8Click, this);
        this.f100_9.on(cc.Node.EventType.TOUCH_END, this.f100_9Click, this);
        this.f500_0.on(cc.Node.EventType.TOUCH_END, this.f500_0Click, this);
        this.f500_1.on(cc.Node.EventType.TOUCH_END, this.f500_1Click, this);
        this.f500_2.on(cc.Node.EventType.TOUCH_END, this.f500_2Click, this);
        this.f500_3.on(cc.Node.EventType.TOUCH_END, this.f500_3Click, this);
        this.f500_4.on(cc.Node.EventType.TOUCH_END, this.f500_4Click, this);
        this.f500_5.on(cc.Node.EventType.TOUCH_END, this.f500_5Click, this);
        this.f500_6.on(cc.Node.EventType.TOUCH_END, this.f500_6Click, this);
        this.f500_7.on(cc.Node.EventType.TOUCH_END, this.f500_7Click, this);
        this.f500_8.on(cc.Node.EventType.TOUCH_END, this.f500_8Click, this);
        this.f500_9.on(cc.Node.EventType.TOUCH_END, this.f500_9Click, this);
        this.f1000_0.on(cc.Node.EventType.TOUCH_END, this.f1000_0Click, this);
        this.f1000_1.on(cc.Node.EventType.TOUCH_END, this.f1000_1Click, this);
        this.f1000_2.on(cc.Node.EventType.TOUCH_END, this.f1000_2Click, this);
        this.f1000_3.on(cc.Node.EventType.TOUCH_END, this.f1000_3Click, this);
        this.f1000_4.on(cc.Node.EventType.TOUCH_END, this.f1000_4Click, this);
        this.f1000_5.on(cc.Node.EventType.TOUCH_END, this.f1000_5Click, this);
        this.f1000_6.on(cc.Node.EventType.TOUCH_END, this.f1000_6Click, this);
        this.f1000_7.on(cc.Node.EventType.TOUCH_END, this.f1000_7Click, this);
        this.f1000_8.on(cc.Node.EventType.TOUCH_END, this.f1000_8Click, this);
        this.f1000_9.on(cc.Node.EventType.TOUCH_END, this.f1000_9Click, this);
        this.g0.on(cc.Node.EventType.TOUCH_END, this.g0Click, this);
        this.g1.on(cc.Node.EventType.TOUCH_END, this.g1Click, this);
        this.g2.on(cc.Node.EventType.TOUCH_END, this.g2Click, this);
        this.g3.on(cc.Node.EventType.TOUCH_END, this.g3Click, this);
        this.g4.on(cc.Node.EventType.TOUCH_END, this.g4Click, this);
        this.g5.on(cc.Node.EventType.TOUCH_END, this.g5Click, this);
        this.g6.on(cc.Node.EventType.TOUCH_END, this.g6Click, this);
        this.g7.on(cc.Node.EventType.TOUCH_END, this.g7Click, this);
        this.g8.on(cc.Node.EventType.TOUCH_END, this.g8Click, this);
        this.g9.on(cc.Node.EventType.TOUCH_END, this.g9Click, this);
    }
    cal() {
        if (this.count < 10) {
            this.count++;
            if (this.goldCount == 20) {
                if (!this.ng_0) {
                    this.check = 0;
                    this.ng_0 = 1;
                }
                else if (!this.ng_1) {
                    this.check = 1;
                    this.ng_1 = 1;
                }
                else if (!this.ng_2) {
                    this.check = 2;
                    this.ng_2 = 1;
                }
                else if (!this.ng_3) {
                    this.check = 3;
                    this.ng_3 = 1;
                }
                else if (!this.ng_4) {
                    this.check = 4;
                    this.ng_4 = 1;
                }
                else if (!this.ng_5) {
                    this.check = 5;
                    this.ng_5 = 1;
                }
                else if (!this.ng_6) {
                    this.check = 6;
                    this.ng_6 = 1;
                }
                else if (!this.ng_7) {
                    this.check = 7;
                    this.ng_7 = 1;
                }
                else if (!this.ng_8) {
                    this.check = 8;
                    this.ng_8 = 1;
                }
                else if (!this.ng_9) {
                    this.check = 9;
                    this.ng_9 = 1;
                }
                this.goldAppear(this.check);
            }
            else {
                this.randNum = Math.floor(Math.random() * 39);
                if (0 <= this.randNum && this.randNum < 20) {
                    if (!this.nr100_0) {
                        this.check = 0;
                        this.nr100_0 = 1;
                    }
                    else if (!this.nr100_1) {
                        this.check = 1;
                        this.nr100_1 = 1;
                    }
                    else if (!this.nr100_2) {
                        this.check = 2;
                        this.nr100_2 = 1;
                    }
                    else if (!this.nr100_3) {
                        this.check = 3;
                        this.nr100_3 = 1;
                    }
                    else if (!this.nr100_4) {
                        this.check = 4;
                        this.nr100_4 = 1;
                    }
                    else if (!this.nr100_5) {
                        this.check = 5;
                        this.nr100_5 = 1;
                    }
                    else if (!this.nr100_6) {
                        this.check = 6;
                        this.nr100_6 = 1;
                    }
                    else if (!this.nr100_7) {
                        this.check = 7;
                        this.nr100_7 = 1;
                    }
                    else if (!this.nr100_8) {
                        this.check = 8;
                        this.nr100_8 = 1;
                    }
                    else if (!this.nr100_9) {
                        this.check = 9;
                        this.nr100_9 = 1;
                    }
                    this.real100Appear(this.check);
                }
                else if (20 <= this.randNum && this.randNum < 24) {
                    if (!this.nr500_0) {
                        this.check = 0;
                        this.nr500_0 = 1;
                    }
                    else if (!this.nr500_1) {
                        this.check = 1;
                        this.nr500_1 = 1;
                    }
                    else if (!this.nr500_2) {
                        this.check = 2;
                        this.nr500_2 = 1;
                    }
                    else if (!this.nr500_3) {
                        this.check = 3;
                        this.nr500_3 = 1;
                    }
                    else if (!this.nr500_4) {
                        this.check = 4;
                        this.nr500_4 = 1;
                    }
                    else if (!this.nr500_5) {
                        this.check = 5;
                        this.nr500_5 = 1;
                    }
                    else if (!this.nr500_6) {
                        this.check = 6;
                        this.nr500_6 = 1;
                    }
                    else if (!this.nr500_7) {
                        this.check = 7;
                        this.nr500_7 = 1;
                    }
                    else if (!this.nr500_8) {
                        this.check = 8;
                        this.nr500_8 = 1;
                    }
                    else if (!this.nr500_9) {
                        this.check = 9;
                        this.nr500_9 = 1;
                    }
                    this.real500Appear(this.check);
                }
                else if (24 <= this.randNum && this.randNum < 26) {
                    if (!this.nr1000_0) {
                        this.check = 0;
                        this.nr1000_0 = 1;
                    }
                    else if (!this.nr1000_1) {
                        this.check = 1;
                        this.nr1000_1 = 1;
                    }
                    else if (!this.nr1000_2) {
                        this.check = 2;
                        this.nr1000_2 = 1;
                    }
                    else if (!this.nr1000_3) {
                        this.check = 3;
                        this.nr1000_3 = 1;
                    }
                    else if (!this.nr1000_4) {
                        this.check = 4;
                        this.nr1000_4 = 1;
                    }
                    else if (!this.nr1000_5) {
                        this.check = 5;
                        this.nr1000_5 = 1;
                    }
                    else if (!this.nr1000_6) {
                        this.check = 6;
                        this.nr1000_6 = 1;
                    }
                    else if (!this.nr1000_7) {
                        this.check = 7;
                        this.nr1000_7 = 1;
                    }
                    else if (!this.nr1000_8) {
                        this.check = 8;
                        this.nr1000_8 = 1;
                    }
                    else if (!this.nr1000_9) {
                        this.check = 9;
                        this.nr1000_9 = 1;
                    }
                    this.real1000Appear(this.check);
                }
                else if (26 <= this.randNum && this.randNum < 36) {
                    if (!this.nf100_0) {
                        this.check = 0;
                        this.nf100_0 = 1;
                    }
                    else if (!this.nf100_1) {
                        this.check = 1;
                        this.nf100_1 = 1;
                    }
                    else if (!this.nf100_2) {
                        this.check = 2;
                        this.nf100_2 = 1;
                    }
                    else if (!this.nf100_3) {
                        this.check = 3;
                        this.nf100_3 = 1;
                    }
                    else if (!this.nf100_4) {
                        this.check = 4;
                        this.nf100_4 = 1;
                    }
                    else if (!this.nf100_5) {
                        this.check = 5;
                        this.nf100_5 = 1;
                    }
                    else if (!this.nr100_6) {
                        this.check = 6;
                        this.nr100_6 = 1;
                    }
                    else if (!this.nf100_7) {
                        this.check = 7;
                        this.nf100_7 = 1;
                    }
                    else if (!this.nf100_8) {
                        this.check = 8;
                        this.nf100_8 = 1;
                    }
                    else if (!this.nf100_9) {
                        this.check = 9;
                        this.nf100_9 = 1;
                    }
                    this.fake100Appear(this.check);
                }
                else if (36 <= this.randNum && this.randNum < 38) {
                    if (!this.nf500_0) {
                        this.check = 0;
                        this.nf500_0 = 1;
                    }
                    else if (!this.nf500_1) {
                        this.check = 1;
                        this.nf500_1 = 1;
                    }
                    else if (!this.nf500_2) {
                        this.check = 2;
                        this.nf500_2 = 1;
                    }
                    else if (!this.nf500_3) {
                        this.check = 3;
                        this.nf500_3 = 1;
                    }
                    else if (!this.nf500_4) {
                        this.check = 4;
                        this.nf500_4 = 1;
                    }
                    else if (!this.nf500_5) {
                        this.check = 5;
                        this.nf500_5 = 1;
                    }
                    else if (!this.nr500_6) {
                        this.check = 6;
                        this.nr500_6 = 1;
                    }
                    else if (!this.nf500_7) {
                        this.check = 7;
                        this.nf500_7 = 1;
                    }
                    else if (!this.nf500_8) {
                        this.check = 8;
                        this.nf500_8 = 1;
                    }
                    else if (!this.nf500_9) {
                        this.check = 9;
                        this.nf500_9 = 1;
                    }
                    this.fake500Appear(this.check);
                }
                else if (this.randNum == 38) {
                    if (!this.nf1000_0) {
                        this.check = 0;
                        this.nf1000_0 = 1;
                    }
                    else if (!this.nf1000_1) {
                        this.check = 1;
                        this.nf1000_1 = 1;
                    }
                    else if (!this.nf1000_2) {
                        this.check = 2;
                        this.nf1000_2 = 1;
                    }
                    else if (!this.nf1000_3) {
                        this.check = 3;
                        this.nf1000_3 = 1;
                    }
                    else if (!this.nf1000_4) {
                        this.check = 4;
                        this.nf1000_4 = 1;
                    }
                    else if (!this.nf1000_5) {
                        this.check = 5;
                        this.nf1000_5 = 1;
                    }
                    else if (!this.nr1000_6) {
                        this.check = 6;
                        this.nr1000_6 = 1;
                    }
                    else if (!this.nf1000_7) {
                        this.check = 7;
                        this.nf1000_7 = 1;
                    }
                    else if (!this.nf1000_8) {
                        this.check = 8;
                        this.nf1000_8 = 1;
                    }
                    else if (!this.nf1000_9) {
                        this.check = 9;
                        this.nf1000_9 = 1;
                    }
                    this.fake1000Appear(this.check);
                }
            }
        }
    }
    real100Appear(num: number) {
        this.goldCount++;
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.r100_0.x = this.randX;
            this.r100_0.y = this.randY;
        }
        else if (num == 1) {
            this.r100_1.x = this.randX;
            this.r100_1.y = this.randY;
        }
        else if (num == 2) {
            this.r100_2.x = this.randX;
            this.r100_2.y = this.randY;
        }
        else if (num == 3) {
            this.r100_3.x = this.randX;
            this.r100_3.y = this.randY;
        }
        else if (num == 4) {
            this.r100_4.x = this.randX;
            this.r100_4.y = this.randY;
        }
        else if (num == 5) {
            this.r100_5.x = this.randX;
            this.r100_5.y = this.randY;
        }
        else if (num == 6) {
            this.r100_6.x = this.randX;
            this.r100_6.y = this.randY;
        }
        else if (num == 7) {
            this.r100_7.x = this.randX;
            this.r100_7.y = this.randY;
        }
        else if (num == 8) {
            this.r100_8.x = this.randX;
            this.r100_8.y = this.randY;
        }
        else if (num == 9) {
            this.r100_9.x = this.randX;
            this.r100_9.y = this.randY;
        }
    }
    real500Appear(num: number) {
        this.goldCount++;
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.r500_0.x = this.randX;
            this.r500_0.y = this.randY;
        }
        else if (num == 1) {
            this.r500_1.x = this.randX;
            this.r500_1.y = this.randY;
        }
        else if (num == 2) {
            this.r500_2.x = this.randX;
            this.r500_2.y = this.randY;
        }
        else if (num == 3) {
            this.r500_3.x = this.randX;
            this.r500_3.y = this.randY;
        }
        else if (num == 4) {
            this.r500_4.x = this.randX;
            this.r500_4.y = this.randY;
        }
        else if (num == 5) {
            this.r500_5.x = this.randX;
            this.r500_5.y = this.randY;
        }
        else if (num == 6) {
            this.r500_6.x = this.randX;
            this.r500_6.y = this.randY;
        }
        else if (num == 7) {
            this.r500_7.x = this.randX;
            this.r500_7.y = this.randY;
        }
        else if (num == 8) {
            this.r500_8.x = this.randX;
            this.r500_8.y = this.randY;
        }
        else if (num == 9) {
            this.r500_9.x = this.randX;
            this.r500_9.y = this.randY;
        }
    }
    real1000Appear(num: number) {
        this.goldCount++;
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.r1000_0.x = this.randX;
            this.r1000_0.y = this.randY;
        }
        else if (num == 1) {
            this.r1000_1.x = this.randX;
            this.r1000_1.y = this.randY;
        }
        else if (num == 2) {
            this.r1000_2.x = this.randX;
            this.r1000_2.y = this.randY;
        }
        else if (num == 3) {
            this.r1000_3.x = this.randX;
            this.r1000_3.y = this.randY;
        }
        else if (num == 4) {
            this.r1000_4.x = this.randX;
            this.r1000_4.y = this.randY;
        }
        else if (num == 5) {
            this.r1000_5.x = this.randX;
            this.r1000_5.y = this.randY;
        }
        else if (num == 6) {
            this.r1000_6.x = this.randX;
            this.r1000_6.y = this.randY;
        }
        else if (num == 7) {
            this.r1000_7.x = this.randX;
            this.r1000_7.y = this.randY;
        }
        else if (num == 8) {
            this.r1000_8.x = this.randX;
            this.r1000_8.y = this.randY;
        }
        else if (num == 9) {
            this.r1000_9.x = this.randX;
            this.r1000_9.y = this.randY;
        }
    }
    fake100Appear(num: number) {
        this.goldCount++;
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.f100_0.x = this.randX;
            this.f100_0.y = this.randY;
        }
        else if (num == 1) {
            this.f100_1.x = this.randX;
            this.f100_1.y = this.randY;
        }
        else if (num == 2) {
            this.f100_2.x = this.randX;
            this.f100_2.y = this.randY;
        }
        else if (num == 3) {
            this.f100_3.x = this.randX;
            this.f100_3.y = this.randY;
        }
        else if (num == 4) {
            this.f100_4.x = this.randX;
            this.f100_4.y = this.randY;
        }
        else if (num == 5) {
            this.f100_5.x = this.randX;
            this.f100_5.y = this.randY;
        }
        else if (num == 6) {
            this.f100_6.x = this.randX;
            this.f100_6.y = this.randY;
        }
        else if (num == 7) {
            this.f100_7.x = this.randX;
            this.f100_7.y = this.randY;
        }
        else if (num == 8) {
            this.f100_8.x = this.randX;
            this.f100_8.y = this.randY;
        }
        else if (num == 9) {
            this.f100_9.x = this.randX;
            this.f100_9.y = this.randY;
        }
    }
    fake500Appear(num: number) {
        this.goldCount++;
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.f500_0.x = this.randX;
            this.f500_0.y = this.randY;
        }
        else if (num == 1) {
            this.f500_1.x = this.randX;
            this.f500_1.y = this.randY;
        }
        else if (num == 2) {
            this.f500_2.x = this.randX;
            this.f500_2.y = this.randY;
        }
        else if (num == 3) {
            this.f500_3.x = this.randX;
            this.f500_3.y = this.randY;
        }
        else if (num == 4) {
            this.f500_4.x = this.randX;
            this.f500_4.y = this.randY;
        }
        else if (num == 5) {
            this.f500_5.x = this.randX;
            this.f500_5.y = this.randY;
        }
        else if (num == 6) {
            this.f500_6.x = this.randX;
            this.f500_6.y = this.randY;
        }
        else if (num == 7) {
            this.f500_7.x = this.randX;
            this.f500_7.y = this.randY;
        }
        else if (num == 8) {
            this.f500_8.x = this.randX;
            this.f500_8.y = this.randY;
        }
        else if (num == 9) {
            this.f500_9.x = this.randX;
            this.f500_9.y = this.randY;
        }
    }
    fake1000Appear(num: number) {
        this.goldCount++;
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.f1000_0.x = this.randX;
            this.f1000_0.y = this.randY;
        }
        else if (num == 1) {
            this.f1000_1.x = this.randX;
            this.f1000_1.y = this.randY;
        }
        else if (num == 2) {
            this.f1000_2.x = this.randX;
            this.f1000_2.y = this.randY;
        }
        else if (num == 3) {
            this.f1000_3.x = this.randX;
            this.f1000_3.y = this.randY;
        }
        else if (num == 4) {
            this.f1000_4.x = this.randX;
            this.f1000_4.y = this.randY;
        }
        else if (num == 5) {
            this.f1000_5.x = this.randX;
            this.f1000_5.y = this.randY;
        }
        else if (num == 6) {
            this.f1000_6.x = this.randX;
            this.f1000_6.y = this.randY;
        }
        else if (num == 7) {
            this.f1000_7.x = this.randX;
            this.f1000_7.y = this.randY;
        }
        else if (num == 8) {
            this.f1000_8.x = this.randX;
            this.f1000_8.y = this.randY;
        }
        else if (num == 9) {
            this.f1000_9.x = this.randX;
            this.f1000_9.y = this.randY;
        }
    }
    goldAppear(num: number) {
        this.goldCount = 0;
        cc.log("hello")
        cc.log(this.node.x);
        cc.log(this.node.y);
        this.randX = this.node.x + Math.floor(Math.random() * 811) - 405;
        this.randY = this.node.y + Math.floor(Math.random() * 308) - 70;
        if (num == 0) {
            this.g0.x = this.randX;
            this.g0.y = this.randY;
        }
        else if (num == 1) {
            this.g1.x = this.randX;
            this.g1.y = this.randY;
        }
        else if (num == 2) {
            this.g2.x = this.randX;
            this.g2.y = this.randY;
        }
        else if (num == 3) {
            this.g3.x = this.randX;
            this.g3.y = this.randY;
        }
        else if (num == 4) {
            this.g4.x = this.randX;
            this.g4.y = this.randY;
        }
        else if (num == 5) {
            this.g5.x = this.randX;
            this.g5.y = this.randY;
        }
        else if (num == 6) {
            this.g6.x = this.randX;
            this.g6.y = this.randY;
        }
        else if (num == 7) {
            this.g7.x = this.randX;
            this.g7.y = this.randY;
        }
        else if (num == 8) {
            this.g8.x = this.randX;
            this.g8.y = this.randY;
        }
        else if (num == 9) {
            this.g9.x = this.randX;
            this.g9.y = this.randY;
        }
    }
    r100_0Click() {
        this.r100_0.x = -750;
        this.nr100_0 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_1Click() {
        this.r100_1.x = -750;
        this.nr100_1 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_2Click() {
        this.r100_2.x = -750;
        this.nr100_2 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_3Click() {
        this.r100_3.x = -750;
        this.nr100_3 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_4Click() {
        this.r100_4.x = -750;
        this.nr100_4 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_5Click() {
        this.r100_5.x = -750;
        this.nr100_5 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_6Click() {
        this.r100_6.x = -750;
        this.nr100_6 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_7Click() {
        this.r100_7.x = -750;
        this.nr100_7 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_8Click() {
        this.r100_8.x = -750;
        this.nr100_8 = 0;
        this.count--;
        this.updateScore(100);
    }
    r100_9Click() {
        this.r100_9.x = -750;
        this.nr100_9 = 0;
        this.count--;
        this.updateScore(100);
    }
    r500_0Click() {
        this.r500_0.x = -750;
        this.nr500_0 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_1Click() {
        this.r500_1.x = -750;
        this.nr500_1 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_2Click() {
        this.r500_2.x = -750;
        this.nr500_2 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_3Click() {
        this.r500_3.x = -750;
        this.nr500_3 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_4Click() {
        this.r500_4.x = -750;
        this.nr500_4 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_5Click() {
        this.r500_5.x = -750;
        this.nr500_5 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_6Click() {
        this.r500_6.x = -750;
        this.nr500_6 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_7Click() {
        this.r500_7.x = -750;
        this.nr500_7 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_8Click() {
        this.r500_8.x = -750;
        this.nr500_8 = 0;
        this.count--;
        this.updateScore(500);
    }
    r500_9Click() {
        this.r500_9.x = -750;
        this.nr500_9 = 0;
        this.count--;
        this.updateScore(500);
    }
    r1000_0Click() {
        this.r1000_0.x = -750;
        this.nr1000_0 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_1Click() {
        this.r1000_1.x = -750;
        this.nr1000_1 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_2Click() {
        this.r1000_2.x = -750;
        this.nr1000_2 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_3Click() {
        this.r1000_3.x = -750;
        this.nr1000_3 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_4Click() {
        this.r1000_4.x = -750;
        this.nr1000_4 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_5Click() {
        this.r1000_5.x = -750;
        this.nr1000_5 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_6Click() {
        this.r1000_6.x = -750;
        this.nr1000_6 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_7Click() {
        this.r1000_7.x = -750;
        this.nr1000_7 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_8Click() {
        this.r1000_8.x = -750;
        this.nr1000_8 = 0;
        this.count--;
        this.updateScore(1000);
    }
    r1000_9Click() {
        this.r1000_9.x = -750;
        this.nr1000_9 = 0;
        this.count--;
        this.updateScore(1000);
    }
    f100_0Click() {
        this.f100_0.x = -750;
        this.nf100_0 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_1Click() {
        this.f100_1.x = -750;
        this.nf100_1 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_2Click() {
        this.f100_2.x = -750;
        this.nf100_2 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_3Click() {
        this.f100_3.x = -750;
        this.nf100_3 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_4Click() {
        this.f100_4.x = -750;
        this.nf100_4 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_5Click() {
        this.f100_5.x = -750;
        this.nf100_5 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_6Click() {
        this.f100_6.x = -750;
        this.nf100_6 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_7Click() {
        this.f100_7.x = -750;
        this.nf100_7 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_8Click() {
        this.f100_8.x = -750;
        this.nf100_8 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f100_9Click() {
        this.f100_9.x = -750;
        this.nf100_9 = 0;
        this.count--;
        this.updateScore(-100);
    }
    f500_0Click() {
        this.f500_0.x = -750;
        this.nf500_0 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_1Click() {
        this.f500_1.x = -750;
        this.nf500_1 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_2Click() {
        this.f500_2.x = -750;
        this.nf500_2 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_3Click() {
        this.f500_3.x = -750;
        this.nf500_3 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_4Click() {
        this.f500_4.x = -750;
        this.nf500_4 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_5Click() {
        this.f500_5.x = -750;
        this.nf500_5 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_6Click() {
        this.f500_6.x = -750;
        this.nf500_6 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_7Click() {
        this.f500_7.x = -750;
        this.nf500_7 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_8Click() {
        this.f500_8.x = -750;
        this.nf500_8 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f500_9Click() {
        this.f500_9.x = -750;
        this.nf500_9 = 0;
        this.count--;
        this.updateScore(-500);
    }
    f1000_0Click() {
        this.f1000_0.x = -750;
        this.nf1000_0 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_1Click() {
        this.f1000_1.x = -750;
        this.nf1000_1 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_2Click() {
        this.f1000_2.x = -750;
        this.nf1000_2 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_3Click() {
        this.f1000_3.x = -750;
        this.nf1000_3 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_4Click() {
        this.f1000_4.x = -750;
        this.nf1000_4 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_5Click() {
        this.f1000_5.x = -750;
        this.nf1000_5 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_6Click() {
        this.f1000_6.x = -750;
        this.nf1000_6 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_7Click() {
        this.f1000_7.x = -750;
        this.nf1000_7 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_8Click() {
        this.f1000_8.x = -750;
        this.nf1000_8 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    f1000_9Click() {
        this.f1000_9.x = -750;
        this.nf1000_9 = 0;
        this.count--;
        this.updateScore(-1000);
    }
    g0Click() {
        this.g0.x = -7500;
        this.ng_0 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g1Click() {
        this.g1.x = -7500;
        this.ng_1 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g2Click() {
        this.g2.x = -7500;
        this.ng_2 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g3Click() {
        this.g3.x = -7500;
        this.ng_3 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g4Click() {
        this.g4.x = -7500;
        this.ng_4 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g5Click() {
        this.g5.x = -7500;
        this.ng_5 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g6Click() {
        this.g6.x = -7500;
        this.ng_6 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g7Click() {
        this.g7.x = -7500;
        this.ng_7 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g8Click() {
        this.g8.x = -7500;
        this.ng_8 = 0;
        this.count--;
        this.updateScore(2500);
    }
    g9Click() {
        this.g9.x = -7500;
        this.ng_9 = 0;
        this.count--;
        this.updateScore(2500);
    }
    updateScore(number) {
        this.score += number;
        if (this.score < 0) this.score = 0;
        this.sLabel.string = this.score.toString();
        if (this.score >= 5000) {
            cc.audioEngine.stopMusic()
            cc.audioEngine.playEffect(this.winSE, false);
            var GameCond = firebase.database().ref('GameCondition');
            GameCond.once('value', (snpashot) => {
                var currentVal = snpashot.val().Task_completed;
                var NewVal = currentVal + 1;
                GameCond.update({
                    Task_completed: NewVal
                })
            })
            //cc.director.loadScene("default_end");
            cc.find("Canvas/T7").active = false;
        }
        else {
            if (number > 0) cc.audioEngine.playEffect(this.rightSE, false);
            else cc.audioEngine.playEffect(this.wrongSE, false);
        }
    }
}
