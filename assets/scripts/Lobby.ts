import Player from "./Player";
declare const firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class Lobby extends cc.Component {

    @property(cc.Label)
    member: cc.Label = null;

    @property(cc.Node)
    start_btn: cc.Node = null;

    public totalmember: number = 5;

    gameStart() {
        var userRef = firebase.database().ref('GameCondition');
        userRef.update({
            EnterGame: true
        })
    }

    start() {
        var userRef = firebase.database().ref('users/' + firebase.auth().currentUser.email.split(".").join("_").replace(/@/g, "_"));
        userRef.once('value').then(snapshot => {
            if (snapshot.val().mario_id == "RedPlayer") {
                cc.find("Canvas/Main Camera/start").active = true;
            }
        })

        var userRef = firebase.database().ref('GameCondition');
        userRef.update({
            GameScene: 'lobby',
            Crew_num: 6,
            Impos_num: 2,
            Task_completed: 0
        })

        //Reset player condition and position
        var otheruserRef = firebase.database().ref('users');
        otheruserRef.once('value', function (snapshot) {
            snapshot.forEach(childSnapshot => {
                var userRef = firebase.database().ref('users/' + childSnapshot.val().email.split(".").join("_").replace(/@/g, "_"));
                userRef.update({
                    isDead: 0,
                    has_voted: false
                })
                if (childSnapshot.val().email == "111@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "111_gmail_com");
                    userRef.update({
                        pos_x: 0,
                        pos_y: 95
                    })
                } else if (childSnapshot.val().email == "123@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "123_gmail_com");
                    userRef.update({
                        pos_x: 100,
                        pos_y: 65
                    })
                } else if (childSnapshot.val().email == "222@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "222_gmail_com");
                    userRef.update({
                        pos_x: 145,
                        pos_y: -20
                    })
                } else if (childSnapshot.val().email == "333@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "333_gmail_com");
                    userRef.update({
                        pos_x: 100,
                        pos_y: -120
                    })
                } else if (childSnapshot.val().email == "444@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "444_gmail_com");
                    userRef.update({
                        pos_x: -5,
                        pos_y: -170
                    })
                } else if (childSnapshot.val().email == "555@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "555_gmail_com");
                    userRef.update({
                        pos_x: -130,
                        pos_y: -120
                    })
                } else if (childSnapshot.val().email == "666@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "666_gmail_com");
                    userRef.update({
                        pos_x: -165,
                        pos_y: -30
                    })
                } else if (childSnapshot.val().email == "777@gmail.com") {
                    var userRef = firebase.database().ref('users/' + "777_gmail_com");
                    userRef.update({
                        pos_x: -120,
                        pos_y: 60
                    })
                }
            })
        });

    }

    update() {
        var userRef = firebase.database().ref('GameCondition');
        userRef.once('value').then(snapshot => {
            if (snapshot.val().EnterGame == true) {
                var userRef = firebase.database().ref('users/' + firebase.auth().currentUser.email.split(".").join("_").replace(/@/g, "_"));
                userRef.once('value').then(snapshot => {
                    if (snapshot.val().mario_id == "RedPlayer" || snapshot.val().mario_id == 'PurplePlayer') {
                        cc.director.loadScene("identity_impostor")
                    } else {
                        cc.director.loadScene("identity_crewmate")
                    }
                })
            }
        })
    }

}

